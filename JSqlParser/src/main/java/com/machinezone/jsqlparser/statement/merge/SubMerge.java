/*
 * #%L
 * JSQLParser library
 * %%
 * Copyright (C) 2004 - 2015 JSQLParser
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
package com.machinezone.jsqlparser.statement.merge;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.PlainSelect;

import java.util.List;

/**
 * Created by kchen on 7/28/15.
 */
public class SubMerge implements Expression {

    boolean useInsert;
    boolean forMatched;

    private List<Column> columns;
    private ItemsList itemsList;
    private List<Expression> expressions;

    @Override
    public void accept(ExpressionVisitor expressionVisitor) {
        expressionVisitor.visit(this);
    }

    public boolean isUseInsert() {
        return useInsert;
    }

    public void setUseInsert(boolean useInsert) {
        this.useInsert = useInsert;
    }

    public boolean isForMatched() {
        return forMatched;
    }

    public void setForMatched(boolean forMatched) {
        this.forMatched = forMatched;
    }

    /**
     * The {@link net.sf.jsqlparser.schema.Column}s in this update (as col1 and
     * col2 in UPDATE col1='a', col2='b')
     *
     * @return a list of {@link net.sf.jsqlparser.schema.Column}s
     */
    public List<Column> getColumns() {
        return columns;
    }

    /**
     * The {@link Expression}s in this update (as 'a' and 'b' in UPDATE
     * col1='a', col2='b')
     *
     * @return a list of {@link Expression}s
     */
    public List<Expression> getExpressions() {
        return expressions;
    }

    public void setColumns(List<Column> list) {
        columns = list;
    }

    public void setExpressions(List<Expression> list) {
        expressions = list;
    }

    public ItemsList getItemsList() {
        return itemsList;
    }

    public void setItemsList(ItemsList list) {
        itemsList = list;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("WHEN ");
        b.append(forMatched ? "" : "NOT ").append("MATCHED THEN ");

        if (!useInsert) {
            b.append("UPDATE SET ");
            for (int i = 0; i < getColumns().size(); i++) {
                if (i != 0) {
                    b.append(", ");
                }
                b.append(columns.get(i)).append(" = ");
                b.append(expressions.get(i));
            }
        } else {
            if (columns != null) {
                b.append(PlainSelect.getStringList(columns, true, true)).append(" ");
                b.append("VALUES ");
                b.append(itemsList);
            }
        }
        return b.toString();
    }

}
