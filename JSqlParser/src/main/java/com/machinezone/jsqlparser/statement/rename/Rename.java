/*
 * #%L
 * JSQLParser library
 * %%
 * Copyright (C) 2004 - 2015 JSQLParser
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
package com.machinezone.jsqlparser.statement.rename;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitor;

import java.util.List;

/**
 * Support the `alter ... rename to ...` syntax
 *
 * @author kchen
 */
public class Rename implements Statement {

    private List<Table> oldTables;
    private List<Table> newTables;

    public List<Table> getOldTables() {
        return oldTables;
    }

    public void setOldTables(List<Table> oldTables) {
        this.oldTables = oldTables;
    }

    public List<Table> getNewTables() {
        return newTables;
    }

    public void setNewTables(List<Table> newTables) {
        this.newTables = newTables;
    }

    @Override
    public void accept(StatementVisitor statementVisitor) {
        statementVisitor.visit(this);
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("ALTER ");
        for (final Table table : oldTables) {
            b.append(table.getFullyQualifiedName()).append(", ");
        }
        b.delete(b.length() - 2, b.length());
        b.append(" RENAME TO ");
        for (final Table table : newTables) {
            b.append(table.getFullyQualifiedName()).append(", ");
        }
        b.delete(b.length() - 2, b.length());
        return b.toString();
    }
}
