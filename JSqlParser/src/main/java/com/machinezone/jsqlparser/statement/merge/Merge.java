/*
 * #%L
 * JSQLParser library
 * %%
 * Copyright (C) 2004 - 2015 JSQLParser
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
package com.machinezone.jsqlparser.statement.merge;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.select.FromItem;

import java.util.List;

/**
 * Created by kchen on 7/28/15.
 */
public class Merge implements Statement {

    private Table table;
    private FromItem fromItem;
    private Expression condition;

    private List<SubMerge> updates;

    @Override
    public void accept(StatementVisitor statementVisitor) {
        statementVisitor.visit(this);
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table name) {
        table = name;
    }

    public FromItem getFromItem() {
        return fromItem;
    }

    public void setFromItem(FromItem fromItem) {
        this.fromItem = fromItem;
    }

    public Expression getCondition() {
        return condition;
    }

    public void setCondition(Expression expression) {
        condition = expression;
    }

    public List<SubMerge> getSubMerges() {
        return updates;
    }

    public void setSubMerges(List<SubMerge> merges) {
        this.updates = merges;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("MERGE INTO ");
        b.append(table.toString());
        b.append("USING ").append(fromItem.toString());
        b.append("ON ").append(condition.toString());
        for (SubMerge update : updates) {
            b.append(update.toString());
        }
        return b.toString();
    }

}
