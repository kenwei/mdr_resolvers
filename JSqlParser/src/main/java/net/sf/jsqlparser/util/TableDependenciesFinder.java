/*
 * #%L
 * JSQLParser library
 * %%
 * Copyright (C) 2004 - 2015 JSQLParser
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
package net.sf.jsqlparser.util;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.SetStatement;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.create.index.CreateIndex;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.view.CreateView;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.execute.Execute;
import net.sf.jsqlparser.statement.insert.Insert;
import com.machinezone.jsqlparser.statement.merge.Merge;
import com.machinezone.jsqlparser.statement.rename.Rename;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author kchen
 */
public class TableDependenciesFinder implements StatementVisitor {

    private TablesNamesFinder finder;

    private Map<String, List<String>> dependencies;

    /**
     * Initializes table names collector.
     */
    protected void init() {
        finder = new TablesNamesFinder();
        dependencies = new HashMap<>();
    }

    protected List<String> filterTables(final List<String> tables, final List<String> filters) {
        return tables.stream().filter(name ->  !filters.contains(name)).collect(Collectors.toList());
    }

    public Map<String, List<String>> getDependencies(final Statement statement) {
        init();
        statement.accept(this);
        return dependencies;
    }

    /**
     * Main entry for this Tool class. A list of found targets is returned.
     *
     * @param delete
     * @return
     */
    public Map<String, List<String>> getDependencies(final Delete delete) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Main entry for this Tool class. A list of found targets is returned.
     *
     * @param insert
     * @return
     */
    public Map<String, List<String>> getDependencies(final Insert insert) {
        init();
        insert.accept(this);
        return dependencies;
    }

    /**
     * Main entry for this Tool class. A list of found targets is returned.
     *
     * @param replace
     * @return
     */
    public Map<String, List<String>> getDependencies(final Replace replace) {
        init();
        replace.accept(this);
        return dependencies;
    }

    /**
     * Main entry for this Tool class. A list of found targets is returned.
     *
     * @param select
     * @return
     */
    public Map<String, List<String>> getDependencies(final Select select) {
        init();
        select.accept(this);
        return dependencies;
    }

    /**
     * Main entry for this Tool class. A list of found targets is returned.
     *
     * @param update
     * @return
     */
    public Map<String, List<String>> getDependencies(final Update update) {
        init();
        update.accept(this);
        return dependencies;
    }

    public Map<String, List<String>> getDependencies(final CreateTable create) {
        init();
        create.accept(this);
        return dependencies;
    }

    public Map<String, List<String>> getDependencies(final Merge merge) {
        init();
        merge.accept(this);
        return dependencies;
    }


    @Override
    public void visit(Select select) {
        final List<String> tables = finder.getTableList(select);
        dependencies.put("", tables);
    }

    @Override
    public void visit(Delete delete) {

    }

    @Override
    public void visit(Update update) {
        final List<String> targets = update.getTables().stream().map(Table::getFullyQualifiedName).collect(Collectors.toList());
        final List<String> sources = filterTables(finder.getTableList(update), targets);
        for (final String source : sources) {
            dependencies.put(source, targets);
        }
    }

    @Override
    public void visit(Insert insert) {
        final String target = insert.getTable().getFullyQualifiedName();
        final List<String> sources = filterTables(finder.getTableList(insert), Arrays.asList(target));
        for (final String source : sources) {
            dependencies.put(source, Arrays.asList(target));
        }
    }

    @Override
    public void visit(Replace replace) {
        final String target = replace.getTable().getFullyQualifiedName();
        final List<String> sources = filterTables(finder.getTableList(replace), Arrays.asList(target));
        for (final String source : sources) {
            dependencies.put(source, Arrays.asList(target));
        }
    }

    @Override
    public void visit(Drop drop) {

    }

    @Override
    public void visit(Truncate truncate) {

    }

    @Override
    public void visit(CreateIndex createIndex) {

    }

    @Override
    public void visit(CreateTable create) {
        final String target = create.getTable().getFullyQualifiedName();
        final List<String> sources = finder.getTableList(create);
        for (final String source : sources) {
            dependencies.put(source, Arrays.asList(target));
        }
    }

    @Override
    public void visit(CreateView create) {
        final String target = create.getView().getFullyQualifiedName();
        final List<String> sources = finder.getTableList(create);
        for (final String source : sources) {
            dependencies.put(source, Arrays.asList(target));
        }
    }

    @Override
    public void visit(Alter alter) {
        //alter.getTable()
    }

    @Override
    public void visit(Statements stmts) {

    }

    @Override
    public void visit(Execute execute) {

    }

    @Override
    public void visit(SetStatement set) {

    }

    @Override
    public void visit(Merge merge) {
        final String target = merge.getTable().getFullyQualifiedName();
        final List<String> sources = finder.getTableList(merge.getFromItem());
        for (final String source : sources) {
            dependencies.put(source, Arrays.asList(target));
        }
    }

    @Override
    public void visit(Rename rename) {
        List<Table> oldTables = rename.getOldTables();
        for (int i = 0; i < oldTables.size(); i++) {
            final Table table = oldTables.get(i);
            dependencies.put(table.getFullyQualifiedName(),
                             Arrays.asList(rename.getNewTables().get(i).getFullyQualifiedName()));
        }

    }
}
