package com.machinezone.jsqlparser

import net.sf.jsqlparser.parser.CCJSqlParserUtil
import net.sf.jsqlparser.util.TablesNamesFinder
import org.scalatest.{Matchers, WordSpec}

/**
 * Created by kchen on 8/19/15.
 */
class JSQLParserSpec extends WordSpec with Matchers {

  "select" should {

    "be parsed" in {
      val sql =
        """with aa as (select * from temp.tbl_first)
          |select
          |  aa.x, bb.y
          |from aa, temp.tbl_second bb, (
          | select * from temp.tbl_third
          |) as cc
          |where bb.id = aa.id
          |  and bb.id = cc.id
        """.stripMargin

      val finder = new TablesNamesFinder()
      val tables = finder.getTableList(CCJSqlParserUtil.parse(sql))

      tables should not be empty
      tables should (contain allOf ("temp.tbl_first", "temp.tbl_second", "temp.tbl_third"))
    }

    "also be parsed while using group as alias" in {
      val sql =
        """with aa as (select id, x as group from temp.tbl_first)
          |select
          |  "group", bb.y
          |from aa, temp.tbl_second bb, (
          | select * from temp.tbl_third
          |) as cc
          |where bb.id = aa.id
          |  and bb.id = cc.id
          |group
          |by
          |aa.x, bb.y
          |
        """.stripMargin

      val finder = new TablesNamesFinder()
      val tables = finder.getTableList(CCJSqlParserUtil.parse(sql))

      tables should not be empty
      tables should (contain allOf ("temp.tbl_first", "temp.tbl_second", "temp.tbl_third"))
    }

    "as well be parsed while comments have been placed inbetween" in {
      val sql =
        """with aa as (select id, "group" as x from temp.tbl_first)
          |select
          |  aa.x as
          |-- some other comments
          |-- again
          |  group, bb.y
          |from aa, temp.tbl_second bb, (
          | select * from temp.tbl_third
          |) as cc
          |where bb.id = aa.id
          |  and bb.id = cc.id
          |group
          |
          |-- aa
          |/* this is
          |   a comment
          |*/
          |by
          |
          |aa.x, bb.y
          |
        """.stripMargin

      val finder = new TablesNamesFinder()
      val tables = finder.getTableList(CCJSqlParserUtil.parse(sql))

      tables should not be empty
      tables should (contain allOf ("temp.tbl_first", "temp.tbl_second", "temp.tbl_third"))
    }

  }

  "merge" should {

    "be parsed" in {
      val sql =
        """merge into temp.tbl_first a
         using temp.tbl_second b
         on a.id = b.id
         when matched then update set
            a.name = b.name
         when not matched then
            insert (name)
            values (b.name)
        """.stripMargin

      val finder = new TablesNamesFinder()
      val tables = finder.getTableList(CCJSqlParserUtil.parse(sql))

      tables should not be empty
      tables should (contain allOf ("temp.tbl_first", "temp.tbl_second"))
    }
  }

}
