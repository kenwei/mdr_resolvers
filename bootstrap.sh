#!/bin/sh

sync_gitrepo_to() {
  repoName=$1
  repoUrl=$2
  branch=$3
  destdir=$4

  echo "Syncing up repos - ${repoName} (${repoUrl}) to ${destdir}/${repoName}"

  mkdir -p ${destdir}

  if [ -e ${destdir}/${repoName} ]; then
    git -C ${destdir}/${repoName} fetch
    git -C ${destdir}/${repoName} reset --hard origin/${branch}
  else 
    git clone ${repoUrl} ${destdir}/${repoName}
  fi
}

sync_gitrepo_to bi_tableau git@gitlab.addsrv.com:data-platform/bi_tableau.git master deps
sync_gitrepo_to bi_analytics git@gitlab.addsrv.com:data-platform/bi_analytics.git master deps

sync_gitrepo_to mdr_vertica git@gitlab.addsrv.com:data-platform-taiwan-engineering-team/mdr_vertica.git master sidekick
sync_gitrepo_to mdr_crowdsource git@gitlab.addsrv.com:data-platform-taiwan-engineering-team/mdr_crowdsource.git master sidekick
sync_gitrepo_to python-mock https://github.com/calvinchengx/python-mock.git master sidekick
sync_gitrepo_to sqlparse https://github.com/andialbrecht/sqlparse.git master sidekick

exit 0
