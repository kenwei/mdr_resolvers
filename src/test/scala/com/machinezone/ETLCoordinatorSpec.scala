package com.machinezone

import java.io.{File, FileWriter}

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigValueFactory.{fromIterable, fromMap}
import org.scalatest.{Matchers, WordSpecLike}

import scala.collection.JavaConversions._

/**
 * Created by kchen on 8/18/15.
 */
class ETLCoordinatorSpec extends TestKit(ActorSystem("testSystem"))
with ImplicitSender with WordSpecLike with Matchers {

  def withFakeConfig(testCode: (String) => Any): Unit = {
    val file = File.createTempFile("test", "etl")
    val writer = new FileWriter(file)
    try {
      writer.write(
        """{source: { content: { value: 'name,surname
          |Jay,Miner
          |Jay,Test' } },
          |extractor : { row: {} },
          |transformers: [{csv: {}},
          |               {vertex: {class:'V'}},
          |               {flow:{operation:'skip',
          |                      if: 'name <> 'Jay''}},
          |               {field:{fieldName:'name', value:'3'}}],
          |loader: { orientdb: { dbURL: 'memory:ETLBaseTest', dbType:'graph' } } }""".stripMargin)
    } finally writer.close()

    testCode(file.getAbsolutePath)
  }

  "ETL processor" should {

    "not be executed " in {
      TestActorRef[ETLCoordinator] ! StartETLMsg(
        ConfigFactory.empty().withValue("etl", fromMap(Map("configurations" -> fromIterable(List())))))
      expectMsg(ETLCompletedMsg(Map()))
    }

    "be executed" in withFakeConfig { fakeConfig =>
      TestActorRef[ETLCoordinator] ! StartETLMsg(
        ConfigFactory.empty().withValue("etl", fromMap(Map("configurations" -> fromIterable(List(fakeConfig))))))
      expectMsgPF() {
        case ETLCompletedMsg(stat) => !stat.isEmpty
      }
    }

    "be failed" in {
      TestActorRef[ETLCoordinator] ! StartETLMsg(
        ConfigFactory.empty().withValue("etl", fromMap(Map("configurations" -> fromIterable(List("inexist.json"))))))
      expectMsg(ETLFailedMsg())
    }

  }

}
