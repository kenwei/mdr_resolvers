package com.machinezone

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, ImplicitSender, TestKit}
import com.typesafe.config.{ConfigValueFactory, ConfigFactory}
import org.scalatest._
import scala.collection.JavaConversions._

/**
 * Created by ychen on 8/18/15.
 */
class GitFetcherSpec extends TestKit(ActorSystem("testSystem")) with ImplicitSender with WordSpecLike with MustMatchers {

  "A git fetcher" must {
    "send back true" in {
      val actorRef = TestActorRef[GitFetcher]
      val goodConfig = ConfigFactory.empty()
                          .withValue("resolvers", ConfigValueFactory.fromIterable(
                          List(
                            ConfigValueFactory.fromMap(Map("name" -> "tableau", "repo" -> "./deps/bi_tableau/"))
                          )))
      actorRef ! RepoUpdateMsg(goodConfig)
      expectMsg(true)
    }
  }


  "A git fetcher receiving bad config" must {
    "send back false" in {
      val actorRef = TestActorRef[GitFetcher]
      val badConfig = ConfigFactory.empty()
                          .withValue("resolvers", ConfigValueFactory.fromIterable(
                          List(
                            ConfigValueFactory.fromMap(Map("name" -> "bad_name", "repo" -> "bad_path"))
                          )))
      actorRef ! RepoUpdateMsg(badConfig)
      expectMsg(false)
    }
  }

}
