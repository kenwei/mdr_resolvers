package com.machinezone

import java.io.{BufferedWriter, FileWriter, File}

import org.scalatest._
import com.orientechnologies.orient.core.record.impl.ODocument
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx
import com.orientechnologies.orient.core.sql.OCommandSQL
import com.machinezone.PostETLProcessor.dbWrapper

abstract class PostETLBase extends FlatSpec with Matchers with BeforeAndAfterEach with PrivateMethodTester {
  protected def getTestDatabase = {
    val db: ODatabaseDocumentTx = new ODatabaseDocumentTx("plocal:./databases/MDR_postetl_spec")
    db.open("admin", "admin") // Let's assume it exists
    db
  }

  protected def resetTestDatabase() = {
    val db: ODatabaseDocumentTx = new ODatabaseDocumentTx("plocal:./databases/MDR_postetl_spec")

    if (db.exists) {
      // existing, drop it
      db.open("admin", "admin")
      db.drop()
    }
    db.create()
    db.close()
  }

  protected def resetAndGetTestDatabase = {
    resetTestDatabase()
    getTestDatabase
  }
}

class PostETLTagZonePropertySpec extends PostETLBase {
  val MDR_CLASSES = List("DatameerImportJob", "DatameerSheet", "DatameerExportJob",
    "DataFile", "VerticaTable", "VerticaView", "VerticaStageTable", "TableauDatasource", "TableauWorksheet")
  val tagZonePropertyPrivateFunc = PrivateMethod[String]('tagZoneProperty)

  override def beforeEach(configMap: ConfigMap): Unit = {
    val db = resetAndGetTestDatabase

    // NOTE: Schema changes are not transactional so OCommandScript doesn't work here.
    db.command(new OCommandSQL(s"CREATE CLASS DataNode")).execute().asInstanceOf[Int]
    for (cls <- MDR_CLASSES) {
      db.command(new OCommandSQL(s"CREATE CLASS $cls EXTENDS DataNode")).execute().asInstanceOf[Int]
    }

    MDR_CLASSES.zipWithIndex.map({case (cname, idx) => 0.to(idx).map(cnt => {
      db.command(new OCommandSQL(s"INSERT INTO $cname (name) VALUES ('$cname value $cnt')")).execute[ODocument]()
    })})

    db.close()
  }

  "tagZoneProperty" should "tag proper zone to each DataNode" in {
    val db = getTestDatabase

    PostETLProcessor invokePrivate tagZonePropertyPrivateFunc(db)

    List(1, 2, 3, 4, 18, 0, 8, 9).zipWithIndex.foreach { case (cnt, idx) =>
      db.queryBySql(s"SELECT FROM DataNode WHERE zone = ${idx + 1}") should have length cnt
    }
  }

  "tagZoneProperty" should "tag zone 6 to Vertica DataNode with special name prefix" in {
    val db = getTestDatabase

    db.command(new OCommandSQL(s"INSERT INTO VerticaTable (name) VALUES ('mk_tableau.test1')")).execute()
    db.command(new OCommandSQL(s"INSERT INTO VerticaView (name) VALUES ('mk_tableau.test2')")).execute()
    db.command(new OCommandSQL(s"INSERT INTO VerticaStageTable (name) VALUES ('analysis_export.hello')")).execute()

    PostETLProcessor invokePrivate tagZonePropertyPrivateFunc(db)

    val z5r = db.queryBySql(s"SELECT FROM DataNode WHERE zone = 5")
    z5r should have length 18 // 5 + 6 + 7

    val z6r = db.queryBySql(s"SELECT FROM DataNode WHERE zone = 6")
    z6r should have length 3 // only 3 nodes created above
  }
}

class PostETLMergeUserContributedPropsSpec extends PostETLBase {
  val mergeUserContributedPropsFunc = PrivateMethod[Map[String, Int]]('mergeUserContributedProperties)
  val mergedOutputTestFilePath = sys.props("java.io.tmpdir") + "/postetl.out"

  override def beforeEach(configMap: ConfigMap): Unit = {
    val db = resetAndGetTestDatabase
    // NOTE: Schema changes are not transactional so OCommandScript doesn't work here.
    db.command(new OCommandSQL(s"CREATE CLASS DataNode")).execute().asInstanceOf[Int]
    db.command(new OCommandSQL(s"CREATE PROPERTY DataNode.columns EMBEDDEDMAP")).execute().asInstanceOf[Int]
    db.close()

    // clean up file
    new File(mergedOutputTestFilePath).delete()
  }

 "mergeUserContributedPropsFunc" should "generate merged output to specified file" in {
   val db = getTestDatabase

   new File(mergedOutputTestFilePath) should not be 'exists

   // create a dummy input
   val bw = new BufferedWriter(new FileWriter(new File(mergedOutputTestFilePath)))
   bw.write("a\t= foo")
   bw.close()

   // seed some DataNodes
   db.command(new OCommandSQL(
     s"""INSERT INTO DataNode (name, columns) VALUES ('test1', {"cola": {}})""".stripMargin)).execute()

   val result = PostETLProcessor invokePrivate mergeUserContributedPropsFunc(
     db, mergedOutputTestFilePath, mergedOutputTestFilePath)

   result should not be empty
   result should contain ("total", 2)
   result should contain ("markAsDeleted", 1)

   new File(mergedOutputTestFilePath) shouldBe 'exists
 }

 "mergeUserContributedPropsFunc" should "prefer user provided values over resolved values" in {
   val db = getTestDatabase

   // create a dummy input
   val bw = new BufferedWriter(new FileWriter(new File(mergedOutputTestFilePath)))
   bw.write("test1::cola::type\t= integer\n")
   bw.write("test1::cola::desc\t= something else\n")
   bw.close()

   // seed some DataNodes
   db.command(new OCommandSQL(
     s"""INSERT INTO
        |  DataNode (name, columns)
        |VALUES
        |  ('test1', {"cola": {"type": "str", "desc": "haha"}})""".stripMargin)).execute()

   val stats = PostETLProcessor invokePrivate mergeUserContributedPropsFunc(
     db, mergedOutputTestFilePath, mergedOutputTestFilePath)

   stats should not be empty
   stats should contain ("total", 2)
   stats should contain ("markAsDeleted", 0)

   val mergedProps = PostETLProcessor.loadPropsFromFile(mergedOutputTestFilePath)
   mergedProps should contain ("test1::cola::type", "integer")
   mergedProps should contain ("test1::cola::desc", "something else")
 }

 "mergeUserContributedPropsFunc" should "mark user defined properties not existed in resolved ones as DELETED" in {
   // create a dummy input
   val bw = new BufferedWriter(new FileWriter(new File(mergedOutputTestFilePath)))
   bw.write("test1::cola::type\t= integer\n")
   bw.write("test1::cola::desc\t= something else\n")
   bw.close()

   val stats = PostETLProcessor invokePrivate mergeUserContributedPropsFunc(
     getTestDatabase, mergedOutputTestFilePath, mergedOutputTestFilePath)

   stats should not be empty
   stats should contain ("total", 0)
   stats should contain ("markAsDeleted", 2)

   val mergedProps = PostETLProcessor.loadPropsFromFile(mergedOutputTestFilePath)
   mergedProps should contain ("test1::cola::type", s"${PostETLProcessor.strMarkAsDeleted} integer")
   mergedProps should contain ("test1::cola::desc", s"${PostETLProcessor.strMarkAsDeleted} something else")
 }

 "mergeUserContributedPropsFunc" should "set UNKNOWN to resolved properties as the default value" in {
   val db = getTestDatabase

   // create a dummy input
   new File(mergedOutputTestFilePath).createNewFile()

   // seed some DataNodes
   db.command(new OCommandSQL(
     s"""INSERT INTO
        |  DataNode (name, columns)
        |VALUES
        |  ('test1', {"cola": {}})""".stripMargin)).execute()

   val stats = PostETLProcessor invokePrivate mergeUserContributedPropsFunc(
     db, mergedOutputTestFilePath, mergedOutputTestFilePath)

   stats should not be empty
   stats should contain ("total", 2)
   stats should contain ("markAsDeleted", 0)

   val mergedProps = PostETLProcessor.loadPropsFromFile(mergedOutputTestFilePath)
   mergedProps should contain ("test1::cola::type", s"${PostETLProcessor.strMarkAsUnknown}")
   mergedProps should contain ("test1::cola::desc", s"${PostETLProcessor.strMarkAsUnknown}")
 }

  "mergeUserContributedPropsFunc" should "manage description properly" in {
    val db = getTestDatabase

    // create a dummy input
    val bw = new BufferedWriter(new FileWriter(new File(mergedOutputTestFilePath)))
    bw.write("test3::!!__DESCRIPTION__!!\t= a sample description\n")
    bw.close()

    // seed some DataNodes
    db.command(new OCommandSQL(
      s"""INSERT INTO
         |  DataNode (name, columns)
         |VALUES
         |  ('test1', {"cola": {}})""".stripMargin)).execute()
    db.command(new OCommandSQL(
      s"""INSERT INTO
         |  DataNode (name, description)
         |VALUES
         |  ('test2', 'should be discarded')""".stripMargin)).execute()
    db.command(new OCommandSQL(
      s"""INSERT INTO
         |  DataNode (name, description)
         |VALUES
         |  ('test3', 'should be overwritten')""".stripMargin)).execute()

    val stats = PostETLProcessor invokePrivate mergeUserContributedPropsFunc(
      db, mergedOutputTestFilePath, mergedOutputTestFilePath)

    stats should not be empty
    stats should contain ("total", 2)
    stats should contain ("markAsDeleted", 0)

    val mergedProps = PostETLProcessor.loadPropsFromFile(mergedOutputTestFilePath)
    mergedProps should contain ("test1::!!__DESCRIPTION__!!", s"${PostETLProcessor.strMarkAsUnknown}")
    mergedProps should contain ("test2::!!__DESCRIPTION__!!", s"${PostETLProcessor.strMarkAsUnknown}") //file always win
    mergedProps should contain ("test3::!!__DESCRIPTION__!!", "a sample description")
  }

 "loadPropsFromFile" should "ignore lines in bad formats" in {
   val bw = new BufferedWriter(new FileWriter(new File(mergedOutputTestFilePath)))
   bw.write("test1::cola::type = integer\n")
   bw.write("test1::cola::desc\t = something else\n")
   bw.write("test1::cola::desc\t= something else\t= haha\n")
   bw.close()

   val mergedProps = PostETLProcessor.loadPropsFromFile(mergedOutputTestFilePath)

   mergedProps shouldBe empty
 }
}