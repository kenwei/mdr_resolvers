/**
 * Created by ychen on 7/22/15.
 */

import akka.actor.{ActorSystem, Props}
import akka.dispatch.ExecutionContexts._
import akka.pattern.ask
import akka.util.Timeout
import com.machinezone.util.AppConfig
import com.machinezone._
import org.slf4j.LoggerFactory

import scala.concurrent.duration._

object HappyScala extends App {

  protected lazy val log = LoggerFactory.getLogger(this.getClass)

  override def main(args: Array[String]) {

    implicit val ec = global
    implicit val timeout = Timeout(15 minutes)
    val system = ActorSystem("resolver")

    val meta = system.actorOf(Props(new MetaAnalyzer), "meta")
    val config = AppConfig()
    meta ? StartAnalyzingMsg(config) map { result =>
      log.info("Resolver execution result => " + result)

      val etl = system.actorOf(Props(new ETLCoordinator), "etl")
      etl ? StartETLMsg(config) map { msg =>
        msg match {
          case ETLCompletedMsg(stats) =>
            log.info("ETL execution result => " + stats)
            Thread.sleep(500)
          case _ =>
        }
        PostETLProcessor.process(config) // with side effects
        system.terminate()
      } onFailure { case x => {
        log.error("Exception in ETL processing: ", x)
        system.terminate()
      }}
    } onFailure {
      case x => {
        log.error("Exception in meta analyzing: ", x)
        system.terminate()
      }
    }
  }
}