package com.machinezone

import java.util.Date

import akka.actor.{Actor, ActorRef}
import com.orientechnologies.orient.etl.OETLProcessorFactory
import com.typesafe.config.Config
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}


/** Messages **/
case class StartETLMsg(config: Config)

case class ETLCompletedMsg(state: Map[String, _])

case class ETLFailedMsg()

/**
 * Coordinates ETL processor to execute resolved data.
 */
class ETLCoordinator extends Actor {

  protected lazy val log = LoggerFactory.getLogger(this.getClass)

  private var trigger: Option[ActorRef] = None

  override def receive: Receive = {
    case StartETLMsg(config) =>
      trigger = Some(sender)

      val enabled = if (config.hasPath("etl.enabled")) config.getBoolean("etl.enabled") else true
      val targets: List[String] =
        if (enabled)
          config.getStringList("etl.configurations").asScala.toList
        else
          List.empty[String]

      Try(targets.foldLeft(Map[String, Any]()) { (m, f) =>
          m + performETLProcess(f)
        }) match {
        case Success(stats) =>
          sender ! ETLCompletedMsg(stats)
        case Failure(e) =>
          log.warn("Fail to execute ETL operation", e)
          sender ! ETLFailedMsg()
      }
    case _ => log.debug("Unknown message")
  }


  def performETLProcess(configFile:String) = {
    val stats = OETLProcessorFactory.createInstance(configFile).execute.getStats
    configFile -> Map(
      "endAt" -> new Date(stats.lastLap),
      "warnings" -> stats.warnings.get(),
      "errors" -> stats.errors.get()
    )
  }

}
