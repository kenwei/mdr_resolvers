package com.machinezone

import java.io.File

import akka.actor.Actor
import com.typesafe.config.Config
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._
import scala.sys.process._

case class RepoUpdateMsg(config: Config)

/**
 * Created by ychen on 8/17/15.
 */
class GitFetcher extends Actor {

  protected lazy val log = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case RepoUpdateMsg(config) => sender ! updateDeps(config)
  }

  def updateDeps(config: Config): Boolean = {

    config.getConfigList("resolvers").map(cfg => {
      val localRepo = new File(cfg.getString("repo")).getAbsolutePath
      val repoName = localRepo.split("/").last

      lazy val cloneCmd = s"git clone git@gitlab.addsrv.com:data-platform/$repoName.git $localRepo" !
      lazy val updateCmd = s"git -C $localRepo pull $localRepo" !

      log.debug(s"== updateDeps == DepsRoot = $localRepo")
      log.debug(s"== updateDeps == repo = $repoName")

      if ((s"test -d $localRepo" !) == 0) {
        log.info(s"== updateDeps == $repoName exists, trying to update this repo.")
        updateCmd
      } else {
        log.info(s"== updateDeps == Missing $repoName, cloning this repo to local.")
        cloneCmd
      }
    }).forall(_ == 0)
  }

}
