package com.machinezone

import java.io.{BufferedWriter, FileWriter, File}

import com.orientechnologies.orient.core.command.script.OCommandScript
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx
import com.orientechnologies.orient.core.exception.OSchemaException
import com.orientechnologies.orient.core.id.{ORID, ORecordId}
import com.orientechnologies.orient.core.metadata.schema.OType
import com.orientechnologies.orient.core.record.impl.ODocument
import com.orientechnologies.orient.core.sql.OCommandSQL
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException
import com.typesafe.config.Config
import org.slf4j.LoggerFactory
import scala.collection.JavaConverters._
import scala.io.Source
import java.util.{List => JList}


/**
 * PostETLProcessor contains ETL-like processes that can't be implemented easily and clearly with OrientDB ETL.
 */

object PostETLProcessor {
  protected lazy val log = LoggerFactory.getLogger(this.getClass)

  type PropDict = Map[String, String]
  object PropDict { def apply() = Map[String, String]() }

  implicit def dbWrapper(db: ODatabaseDocumentTx) = new {
    def queryBySql[T](sql: String, params: AnyRef*): List[T] = {
      val params4java = params.toArray
      val results: java.util.List[T] = db.query(new OSQLSynchQuery[T](sql), params4java: _*)
      results.asScala.toList
    }
  }

  def process(cfg: Config): Unit = {
    if (cfg.getBoolean("postetl.enabled")) {
      val db: ODatabaseDocumentTx = new ODatabaseDocumentTx(cfg.getString("dburl"))

      if (db.exists) {
        // TODO: All db configs should be configurable.
        db.open("admin", "admin")

        insertEffectiveFlowsToEdges(db)
        tagZoneProperty(db)
        mergeUserContributedProperties(db)

        db.close()
      } else {
        throw new RuntimeException("MDR database doesn't exist, please enable ETL to generate it.")
      }
    } else {
      log.info("PostETL is disabled.")
    }
  }

  /**
   * This process creates new edges to assist traversing graph without transient nodes.
   *
   * NOTE: The current algorithm mainly based on traversal starting from non-transient nodes. It's quite straightforward
   * and should produce correct result. Not sure whether we can get better performance the other way around (i.e. by
   * traversing from transient nodes, given the number of transient nodes is smaller than of non-transient ones).
   *
   * XXX: OrientDB outputs lots of warnings about "Element 'XXX' not found in traverse history", these should be safe
   * to ignore, as OrientDB tries to pop up a node we never traversed from $history. This is probably caused by the way
   * we write the query.
   *
   * @param db the database (of course)
   */
  private def insertEffectiveFlowsToEdges(db: ODatabaseDocumentTx) = {
    log.info("Starting insertEffectiveFlowsToEdges")

    try {
      log.info("Creating class and index for effectiveFlowsTo")
      db.command(new OCommandSQL("CREATE CLASS effectiveFlowsTo EXTENDS flowsTo")).execute()
      db.command(new OCommandSQL("CREATE PROPERTY effectiveFlowsTo.in link")).execute()
      db.command(new OCommandSQL("CREATE PROPERTY effectiveFlowsTo.out link")).execute()
      db.command(new OCommandSQL("CREATE INDEX effectiveFlows.in_n_out ON effectiveFlowsTo (in, out) UNIQUE")).execute()

      db.command(new OCommandSQL("UPDATE DataNode SET transient=ifnull(transient, false)")).execute()
    } catch {
      case schemaProblem: OSchemaException => log.info("Is effectiveFlowsTo already there? " + schemaProblem)
      case e: Throwable => {
        log.error("Unexpected error when preparing schema and data: ", e)
        throw e
      }
    }

    log.info("Collecting information from database...")
    val result = db.queryBySql[ODocument](
      """SELECT FROM
        |   (SELECT FLATTEN(IN("flowsTo")) FROM DataNode WHERE transient = true)
        |WHERE transient = false
        |""".stripMargin)

    /* Build a set of non-transient node connected with transient nodes */
    val bridgeStarts = result.map(doc => doc.field("@rid").asInstanceOf[ORecordId])
    log.info("Processing " + bridgeStarts.length + " data nodes.")

    /* Visit every non-transient nodes and create new edges to bypass downstream transient nodes */
    for (startRid <- bridgeStarts;
         /* Traverse all involved downstream transient nodes and collect their outs
            until we meet non-transient nodes */
         outsOriginateFromTransOnPaths <- collectOutsFromSubsequentTransNodes(db, startRid);
         /* Only need to create edges to non-transient outs */
         endRid <- filterTransientOuts(outsOriginateFromTransOnPaths)
    ) yield {
      createEffectiveFlowsTo(db, startRid.getIdentity, endRid.getIdentity)
    }

    log.info("Finished!")

    // --- helper functions below ---

    // Collect outs from transient nodes on paths
    def collectOutsFromSubsequentTransNodes(db: ODatabaseDocumentTx, startRid: ORID): List[List[ODocument]] = {
      val queryStr =
        s"""
          |SELECT OUT("flowsTo") AS flowsToRids FROM (
          |   TRAVERSE OUT('flowsTo') FROM (
          |       SELECT OUT('flowsTo') FROM DataNode WHERE @rid = '$startRid'
          |   ) WHILE transient = true
          |)
        """.stripMargin
      log.debug("query = " + queryStr)
      db.queryBySql[ODocument](queryStr).map(
        _.field("flowsToRids").asInstanceOf[java.util.List[ODocument]].asScala.toList)
    }

    // Remove all transient nodes, we only care about non-transient ends
    def filterTransientOuts(flowsToRids: List[ODocument]): Set[ODocument] =
      flowsToRids.toSet[ODocument].filter(! _.field("transient").asInstanceOf[Boolean])

    // Create new edge
    def createEffectiveFlowsTo(db: ODatabaseDocumentTx, startRid: ORID, endRid: ORID) = {
      val createEdgeSt: String =
        "CREATE EDGE effectiveFlowsTo FROM " + startRid + " TO " + endRid

      try {
        log.info(createEdgeSt)
        db.command(new OCommandSQL(createEdgeSt)).execute()
        log.debug("CREATE EDGE: DONE")
      } catch {
        case dupExp: ORecordDuplicatedException =>
          log.info("CREATE EDGE: Edge existed. Ignore.")
      }
    }
  }

  /**
   * Zone property is mainly for helping UI place data nodes in an order makes sense to human. Smaller zone number
   * indicates this is a zone closer to data upstream; on the other hand, bigger zone number means it is closer to
   * downstream.
   *
   * @param db the database (of course)
   */
  private def tagZoneProperty(db: ODatabaseDocumentTx) = {
    val condVerticaClasses = "(@class = 'VerticaStageTable' OR @class = 'VerticaTable' OR @class = 'VerticaView')"
    val condMktAnalysisSchema = """(name LIKE "analysis_export.%" OR name LIKE "mk_tableau.%")"""
    db.command(new OCommandScript("sql",
      s"""BEGIN
         |LET z1 = UPDATE DataNode SET zone = 1 WHERE @class = 'DatameerImportJob'
         |LET z2 = UPDATE DataNode SET zone = 2 WHERE @class = 'DatameerSheet'
         |LET z3 = UPDATE DataNode SET zone = 3 WHERE @class = 'DatameerExportJob'
         |LET z4 = UPDATE DataNode SET zone = 4 WHERE @class = 'DataFile'
         |LET z5 = UPDATE DataNode SET zone = 5 WHERE $condVerticaClasses
         |LET z6 = UPDATE DataNode SET zone = 6 WHERE $condVerticaClasses AND $condMktAnalysisSchema
         |LET z7 = UPDATE DataNode SET zone = 7 WHERE @class = 'TableauDatasource'
         |LET z8 = UPDATE DataNode SET zone = 8 WHERE @class = 'TableauWorksheet'
         |COMMIT RETRY 100
         |RETURN [$$z1, $$z2, $$z3, $$z4, $$z5, $$z6, $$z7, $$z8]
      """.stripMargin)).execute().asInstanceOf[JList[Integer]].asScala.zipWithIndex.foreach {
        case (r, z) => log.info(s"Tagging zone ${z + 1} to $r nodes.")
    }
  }

  /**
   *
   */
  val defaultCrowdSourceInputPath = "sidekick/mdr_crowdsource/datanode.column.info/main.properties"
  val sep = "\t="
  val strMarkAsDeleted = "-- MARKED AS DELETED --"
  val strMarkAsUnknown = "- UNKNOWN -"

  /**
   * A utility function for loading properties from user specified file.
   *
   * @param path path of this property file
   * @return a properties map loaded from this file
   */
  def loadPropsFromFile(path: String) : PropDict = {
    val propsSource = Source.fromFile(path)
    val props : PropDict = propsSource.getLines().foldLeft(PropDict())((z, l) => {
      val tokens = l.split(sep)
      if (tokens.length != 2) {
        log.warn(s"Ignore bad line (${tokens.length}): $l")
        z
      } else {
        z + ((tokens(0).trim, tokens(1).trim))
      }
    })
    propsSource.close()
    props
  }

  /**
   * This function merges user provided columns properties with our resolved results. It also writes merged results back
   * to database and generate a new output file (or overwrite the original file depends on the parameters).
   *
   * @param db the database instance
   * @param crowdSource path of user input file
   * @param mergedOutput path of output merged file
   * @return a map contains stats of processing result
   */
  private def mergeUserContributedProperties(
      db: ODatabaseDocumentTx, crowdSource: String = defaultCrowdSourceInputPath,
      mergedOutput: String = defaultCrowdSourceInputPath) = {
    type JavaMoM = java.util.Map[String, java.util.Map[String, String]]
    val docsWithColumns = db.queryBySql[ODocument]("SELECT FROM DataNode")

    // Load user contributed properties from a repo in sidekick/.
    var userContributedProps = loadPropsFromFile(crowdSource)

    // Merge columns
    val mergedProps = docsWithColumns.foldRight(PropDict())((odoc, props) => { // ----------- document level
      val docName: String = odoc.field("name")
      val updatedColumns = odoc.field("columns").asInstanceOf[JavaMoM] match {
        case obj if !(obj == null || obj.isEmpty) =>
          val curColumns = obj.asScala

          curColumns.foldLeft(Map[String, PropDict]())((resolvedCols, thisColumn) => { //col lvl
            val (colName, colAttrs) = thisColumn

            val newColAttrs = List("type", "desc").foldLeft(PropDict())((colAttrsMap, key) => {
              val propKey = List(docName, colName, key).mkString("::")
              val propVal = userContributedProps.getOrElse(propKey, colAttrs.getOrDefault(key, strMarkAsUnknown))

              if (log.isDebugEnabled && userContributedProps.contains(propKey)) {
                log.debug(s"$propKey is overrode by user provided value: $propVal")
              }

              // NOTE: eventually userContributedProps will only have those never appear in resolved properties, so we
              //       can mark them as deleted for later examination.
              userContributedProps = userContributedProps - propKey

              colAttrsMap + ((key, propVal))
            })
            resolvedCols + ((colName, newColAttrs))
          })
        case _ => Map() // it's likely a NULL
      }

      // MERGE TO DATABASE HERE:
      odoc.field("columns", updatedColumns.map { case (k, v) => (k, v.asJava) }.asJava, OType.EMBEDDEDMAP)
      odoc.save

      props ++ updatedColumns.foldLeft(PropDict())((resolvedProps, newColumn) => {
        val (colName, colAttrs) = newColumn
        val propsOfThisCol = colAttrs.foldLeft(PropDict())((z, kv) => {
          val propKey = List(docName, colName, kv._1).mkString("::")
          val propVal = kv._2
          z + ((propKey, propVal))
        })
        resolvedProps ++ propsOfThisCol
      })
    })

    // Merge description
    val mergedDescription = docsWithColumns.map(odoc => {
      val docName: String = odoc.field("name")
      val descKey = List(docName, "!!__DESCRIPTION__!!").mkString("::")
      val newDescForThisNode = userContributedProps.getOrElse(descKey, strMarkAsUnknown)

      odoc.field("description", newDescForThisNode)
      odoc.save()

      // remove from known kvs, otherwise it will be marked as deleted
      userContributedProps = userContributedProps - descKey

      (descKey, newDescForThisNode)
    })

    /*
       R: resolved results
       U: users provided properties

         R: + / U: + -> U overrides R
         R: + / U: - -> R (or empty)
         R: - / U: + -> U should be marked as deleted
         R: - / U: - -> nothing
     */
    val mayDeprecatedProps = userContributedProps.map {
      case (k, v) =>
        val untainted_v = v.replaceFirst(s"$strMarkAsDeleted ", "")
        (k, s"$strMarkAsDeleted $untainted_v")
    }

    val bw = new BufferedWriter(new FileWriter(new File(mergedOutput)))
    (mergedDescription ++ mergedProps ++ mayDeprecatedProps).sortBy(_._1).foreach(
      e => bw.write(s"${e._1}$sep ${e._2}\n")
    )
    bw.close()

    log.info(s"Total properties = ${mergedProps.size} / Marked as deleted: ${userContributedProps.size}")

    Map(("total", mergedProps.size), ("markAsDeleted", userContributedProps.size))
  }
}
