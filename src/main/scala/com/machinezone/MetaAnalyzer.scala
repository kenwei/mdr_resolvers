package com.machinezone

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import com.machinezone.resolver._
import com.machinezone.util.Helper
import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.JavaConversions._
import scala.collection.mutable.Set
import scala.concurrent.duration._

case class StartAnalyzingMsg(config: Config)

/**
 * Created by ychen on 8/6/15.
 */
class MetaAnalyzer extends Actor with ActorLogging {

  protected val resolvers = Set[String]()

  private var trigger: Option[ActorRef] = None

  override def receive: Receive = {
    case StartAnalyzingMsg(appConfig) =>
      log.debug(appConfig.toString)

      trigger = Some(sender)
      implicit val timeout = Timeout(25 seconds)

      val enabled = appConfig.getBoolean("resolver.enabled")
      val deps = appConfig.getString("resolver.deps")
      val output = appConfig.getString("resolver.output")
      val workersConfig = if (enabled) { appConfig.getConfig("resolver.workers") } else { ConfigFactory.empty() }

      Helper.createOutputDirectory(output)

      workersConfig.root.entrySet.toList.map(entry => {
        val name = entry.getKey
        val config = appConfig.getConfig(s"resolver.workers.$name")
        Helper.createOutputDirectory(s"$output/$name")
        Helper.createOutputDirectory(s"$output/git")

        // Other dependency resolver(s)
        getResolver(name) match {
          case Some(resolver) =>
            context.actorOf(Props(resolver), name) ! StartResolveRepoMsg(config, deps, output)
            resolvers += name
          case _ =>
            log.warning(s"$name resolver not implemented yet")
        }
      })
      // in case we don't have any available resolver.
      notifyTrigger(resolvers.isEmpty)
    case RepoResolvedMsg() =>
      val resolverName: String = sender.path.name
      resolvers -= resolverName
      log.info(s"Resolver ${resolverName} claims it's done.")
      notifyTrigger(resolvers.isEmpty)
    case RepoEmptyMsg() =>
      val resolverName: String = sender.path.name
      resolvers -= resolverName
      log.info(s"Resolver ${resolverName} claims it has nothing to resolve.")
      notifyTrigger(resolvers.isEmpty)
  }

  private def getResolver(name: String): Option[Class[_ <: Actor]] = {
    name match {
      case "tableau" => Some(classOf[TableauDependencyResolver])
      case "datameer" => Some(classOf[DatameerResolver])
      case "vertica" => Some(classOf[VerticaResolver])
      case _ => None
    }
  }

  private def notifyTrigger(cond: Boolean) = {
    if (cond) {
      log.info("All resolvers have done their task.")
      trigger.map(_ ! "done")
    }
  }

}
