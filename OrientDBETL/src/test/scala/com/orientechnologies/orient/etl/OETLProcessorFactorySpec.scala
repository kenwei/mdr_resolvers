package com.orientechnologies.orient.etl

import java.io.{FileWriter, File}

import org.scalatest.{Matchers, WordSpec}

/**
 * Created by kchen on 9/7/15.
 */
class OETLProcessorFactorySpec extends WordSpec with Matchers {

  def withFakeConfig(testCode: (String) => Any): Unit = {
    val file = File.createTempFile("test", "etl")
    val writer = new FileWriter(file)
    try {
      writer.write(
        """{source: { content: { value: 'name,surname
          |Jay,Miner
          |Jay,Test' } },
          |extractor : { row: {} },
          |transformers: [{csv: {}},
          |               {vertex: {class:'V'}},
          |               {flow:{operation:'skip',
          |                      if: 'name <> 'Jay''}},
          |               {field:{fieldName:'name', value:'3'}}],
          |loader: { orientdb: { dbURL: 'memory:ETLBaseTest', dbType:'graph' } } }""".stripMargin)
    } finally writer.close()

    testCode(file.getAbsolutePath)
  }

  "OETLProcessorFactory" should {
    "create functional instance" in withFakeConfig { config =>

      OETLProcessorFactory.createInstance(config) should not be null

    }
  }

}
