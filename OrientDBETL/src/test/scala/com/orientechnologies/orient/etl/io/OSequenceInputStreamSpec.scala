package com.orientechnologies.orient.etl.io

import java.io._

import org.scalatest.{Matchers, WordSpec}

/**
 * Created by kchen on 9/7/15.
 */
class OSequenceInputStreamSpec extends WordSpec with Matchers {

  def withFakeFiles(testCode: (Seq[String]) => Any): Unit = {
    val paths = Range(0, 2).map { idx =>
      val file = File.createTempFile("test", idx.toString)
      val writer = new FileWriter(file)
      try {
        writer.write(s"hello world #${idx}")
      } finally writer.close()
      file.getAbsolutePath
    }
    testCode(paths)
  }

  "OSequenceInputStream" should {
    "pipeline in multiple input streams" in withFakeFiles { files =>
      val stream = new BufferedReader(new InputStreamReader(new OSequenceInputStream(files.map(f => new FileInputStream(f)):_*)))
      Range(0, 2).foreach { idx =>
        val line = stream.readLine()
        line should be eq s"hello world #${idx}"
      }
    }
  }

}
