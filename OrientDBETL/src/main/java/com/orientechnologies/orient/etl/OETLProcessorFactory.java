package com.orientechnologies.orient.etl;

import com.orientechnologies.common.io.OIOUtils;
import com.orientechnologies.orient.core.command.OBasicCommandContext;
import com.orientechnologies.orient.core.exception.OConfigurationException;
import com.orientechnologies.orient.core.record.impl.ODocument;

import java.io.File;
import java.io.IOException;

/**
 * Created by kchen on 9/7/15.
 */
public class OETLProcessorFactory {

    public static OETLProcessor createInstance(final String configFile) {
        return createInstance(configFile, null);
    }

    public static OETLProcessor createInstance(final String configFile, final OBasicCommandContext conx) {
        final OBasicCommandContext context = conx == null ? createDefaultContext() : conx;

        ODocument cfgGlobal = null;
        ODocument configuration = null;

        try {
            final String config = OIOUtils.readFileAsString(new File(configFile));
            configuration = new ODocument().fromJSON(config, "noMap");
            cfgGlobal = configuration.field("config");
        } catch (IOException e) {
            throw new OConfigurationException("Error on loading config file: " + configFile);
        }

        if (cfgGlobal != null) {
            // INIT ThE CONTEXT WITH GLOBAL CONFIGURATION
            for (String f : cfgGlobal.fieldNames()) {
                context.setVariable(f, cfgGlobal.field(f));
            }
        }

        return new OETLProcessor().parse(configuration, context);
    }

    protected static OBasicCommandContext createDefaultContext() {
        final OBasicCommandContext context = new OBasicCommandContext();
        context.setVariable("dumpEveryMs", 1000);
        return context;
    }

}
