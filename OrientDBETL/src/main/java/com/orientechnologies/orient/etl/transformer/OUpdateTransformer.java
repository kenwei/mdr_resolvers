/*
 *
 *  * Copyright 2010-2014 Orient Technologies LTD (info(at)orientechnologies.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.orientechnologies.orient.etl.transformer;

import com.orientechnologies.orient.core.command.OBasicCommandContext;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.etl.OETLProcessor;

import java.util.Collection;
import java.util.List;

/**
 * Update found records with input.
 */
public class OUpdateTransformer extends OAbstractLookupTransformer {

    protected List<String> ignoredFields;

    @Override
    public void configure(final OETLProcessor iProcessor, final ODocument iConfiguration, OBasicCommandContext iContext) {
        super.configure(iProcessor, iConfiguration, iContext);

        ignoredFields = iConfiguration.field("ignoredFields");
    }

    @Override
    public ODocument getConfiguration() {
        return new ODocument().fromJSON("{parameters:[" + getCommonConfigurationParameters() + ","
                + "{joinFieldName:{optional:false,description:'field name containing the value to join'}},"
                + "{lookup:{optional:false,description:'<Class>.<property> or Query to execute'}},"
                + "{ignoredFields:{optional:false,description:'array of field names to be ignored during the update'}},"
                + "input:['ODocument'],output:'ODocument'}");
    }

    @Override
    public String getName() {
        return "update";
    }

    protected void merge(final ODocument input, final ODocument found) {
        found.merge(input, true, false);
        found.save();
        log(OETLProcessor.LOG_LEVELS.DEBUG, "update record %s with input=%s", found, input);
    }

    @Override
    public Object executeTransform(final Object input) {
        final ODocument inputDoc = ((ODocument) input).copy();

        Object joinValue = inputDoc.field(joinFieldName);
        final Object result = lookup(joinValue, false, true);

        log(OETLProcessor.LOG_LEVELS.DEBUG, "joinValue=%s, lookupResult=%s", joinValue, result);

        if (result != null) {
            if (this.ignoredFields != null) {
                this.ignoredFields.forEach(inputDoc::removeField);
            }

            if (result instanceof Collection) {
                for (final ODocument doc :((Collection<ODocument>) result)) {
                    merge(inputDoc, doc);
                }
            } else {
                merge(inputDoc, (ODocument) result);
            }
        }

        return input;
    }
}
