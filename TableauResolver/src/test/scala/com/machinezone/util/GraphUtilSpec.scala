package com.machinezone.util

import com.machinezone.resolver.DataClass
import com.tinkerpop.blueprints.Direction._
import com.tinkerpop.blueprints.impls.tg.TinkerGraph
import org.scalatest.{Matchers, WordSpec}

/**
 * Created by kchen on 9/17/15.
 */
class GraphUtilSpec extends WordSpec with Matchers {

  "GraphWrapper" should {
    "get or create vertex as expected" in {
      import com.machinezone.util.GraphUtil.GraphWrapper

      val graph = new TinkerGraph()
      (graph ++("a", "test", "test.json")).getId should be ("a")
      (graph ++("a", "test2", "test2.json")).getProperty("class").asInstanceOf[String] should be ("test")
      (graph getOrCreateDataSource("dsTest", "dsTest.json")).getProperty("class").asInstanceOf[String] should be (DataClass.TableauDatasource)
      (graph getOrCreateSheet ("wsTest", "wsTest.json")).getProperty("class").asInstanceOf[String] should be (DataClass.TableauWorksheet)
      (graph getOrCreateTable ("tbTest", "tbTest.json")).getProperty("class").asInstanceOf[String] should be (DataClass.VerticaTable)
    }

    "converting to map as expected" in {
      import com.machinezone.util.GraphUtil._

      val graph = new TinkerGraph()
      val t1 = graph ++("a", "test1", "test1.json")
      graph ++("b", "test2", "test2.json") andLinkTo t1

      graph.toMaps should contain allOf(
        Map(
          "name" -> "a",
          "class" -> "test1",
          "src_files" -> List("test1.json"),
          "links" -> List()
        ),
        Map(
          "name" -> "b",
          "class" -> "test2",
          "src_files" -> List("test2.json"),
          "links" -> List("a")
        ))
    }
  }

  "VertexWrapper" should {
    "work as expected" in {
      import com.machinezone.util.GraphUtil.VertexWrapper

      val graph: TinkerGraph = new TinkerGraph()
      val vertex = graph.addVertex("testable 1")

      vertex.andLinkTo(graph.addVertex("testable 2")).getVertex(OUT) should be (vertex)
    }
  }

}
