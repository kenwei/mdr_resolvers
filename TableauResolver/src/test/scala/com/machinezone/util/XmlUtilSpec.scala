package com.machinezone.util

import java.io.{FileWriter, File}

import org.scalatest.{WordSpec, Matchers}

import scala.xml.XML

/**
 * Created by kchen on 9/15/15.
 */
class XmlUtilSpec extends WordSpec with Matchers {

  def withFakeXMLFile(testCode: (String) => Any): Unit = {
    val file = File.createTempFile("test", ".xml")
    val writer = new FileWriter(file)
    try {
      writer.write(
        """<xml>
          |<a>a</a>
          |</xml>""".stripMargin)
    } finally writer.close()

    testCode(file.getAbsolutePath)
  }

  "StringWrapper" should {
    "works as expected" in {
      import XmlUtil.StringWrapper

      "[abc]".normalized should be ("abc")
      "[[abc]]".normalized should be ("abc")
      "[][]abc[][]".normalized should be ("abc")
      "<abc>".normalized should be ("<abc>")
    }
    "loads XML as expected" in withFakeXMLFile { file =>
      import XmlUtil.StringWrapper

      val (_, _, xml) = file.loadAsXml
      (xml \ "a").text should be ("a")
    }
  }

  "NodeWrapper" should {
    "works as expected" in {
      import XmlUtil.NodeSeqWrapper

      val root = XML.loadString(
        """<root>
          |<node id="1" class="unknown">[Node 1]</node>
          |<edon id="2" type="unknown">Edon 2</edon>
          |</root>""".stripMargin)

      root \>> "node" should be ("Node 1")
      root ~\>> ("node", "edon") should be ("Node 1")
      (root ~\ ("n**e", "edon")).ntext should be ("Edon 2")
      (root \ "node") ~\@ ("name", "class") should be ("unknown")
    }
  }

}
