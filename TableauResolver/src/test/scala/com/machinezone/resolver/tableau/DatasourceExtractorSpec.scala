package com.machinezone.resolver.tableau

import com.machinezone.util.GraphUtil._
import org.scalatest.{Matchers, WordSpec}

import scala.xml.XML

/**
 * Created by kchen on 9/17/15.
 */
class DatasourceExtractorSpec extends WordSpec with Matchers {

  "DatasourceExtractor" should {
    "return correct result" in {
      val xml = XML.loadString(
        """<workbook>
          |  <datasources>
          |   <datasource name="Parameters">
          |   </datasource>
          |   <datasource name="datasource">
          |     <connection>
          |       <relation type="text">
          |         Select a from tbl_a
          |       </relation>
          |       <relation type="table" name="sqlproxy" table="tbl_b">
          |       </relation>
          |       <relation type="table" name="non-sqlproxy" table="tbl_c">
          |       </relation>
          |       <relation type="unknown">
          |       </relation>
          |       <metadata-records>
          |         <metadata-record class="capacity">
          |         </metadata-record>
          |         <metadata-record class="measure">
          |           <remote-name>a</remote-name>
          |           <remote-type>-1</remote-type>
          |           <local-name>[a]</local-name>
          |           <parent-name>[sqlproxy]</parent-name> >
          |           <caption>Playtime</caption>
          |           <local-type>real</local-type>
          |         </metadata-record>
          |         <metadata-record class="measure">
          |           <remote-name>c</remote-name>
          |           <remote-type>-1</remote-type>
          |           <local-name>[c]</local-name>
          |           <remote-alias>a</remote-alias>
          |           <local-type>integer</local-type>
          |           <contains-null>true</contains-null>
          |         </metadata-record>
          |       </metadata-records>
          |     </connection>
          |   </datasource>
          |  </datasources>
          |</workbook>
        """.stripMargin)
      val parsed = DatasourceExtractor("testfile.twb", "test", "testfile.twb", xml)
      parsed.toMaps should contain allOf(
        Map("name" -> "tbl_a",
            "class" -> "VerticaTable",
            "src_files" -> List("testfile.twb"),
            "links" -> List("test/datasource")),
        Map("name" -> "tbl_c",
            "class" -> "VerticaTable",
            "src_files" -> List("testfile.twb"),
            "links" -> List("test/datasource")),
        Map("name" -> "test/datasource",
            "class" -> "TableauDatasource",
            "src_files" -> List("testfile.twb"),
            "links" -> List(),
            "columns" -> Map(
              "Playtime" -> Map("type" -> "real"),
              "c" -> Map("type" -> "integer")
            ))
        )
    }
  }


}
