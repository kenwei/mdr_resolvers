package com.machinezone.resolver.tableau

import com.machinezone.util.GraphUtil._
import org.scalatest.{Matchers, WordSpec}

import scala.xml.XML

/**
 * Created by kchen on 9/18/15.
 */
class WorksheetExtractorSpec extends WordSpec with Matchers {

  "DatasourceExtractor" should {

    "return correct result" in {
      val xml = XML.loadString(
        """<workbook>
          |  <datasources>
          |   <datasource name="Parameters">
          |   </datasource>
          |   <datasource name="datasource">
          |     <connection>
          |       <relation type="text">
          |         Select a from tbl_a
          |       </relation>
          |       <relation type="table" name="sqlproxy" table="tbl_b">
          |       </relation>
          |       <relation type="table" name="non-sqlproxy" table="tbl_c">
          |       </relation>
          |       <relation type="unknown">
          |       </relation>
          |       <metadata-records>
          |         <metadata-record class="capacity">
          |         </metadata-record>
          |         <metadata-record class="measure">
          |           <remote-name>Calculation_234567</remote-name>
          |           <remote-type>-1</remote-type>
          |           <local-name>[c]</local-name>
          |           <caption>Playtime</caption>
          |           <remote-alias>a</remote-alias>
          |           <local-type>integer</local-type>
          |           <contains-null>true</contains-null>
          |         </metadata-record>
          |       </metadata-records>
          |     </connection>
          |   </datasource>
          |  </datasources>
          |  <worksheets>
          |    <worksheet name="sheet1">
          |      <table>
          |       <view>
          |         <datasources>
          |           <datasource name="Parameters"></datasource>
          |           <datasource caption="datasource" name="xyz"></datasource>
          |         </datasources>
          |         <datasource-dependencies>
          |           <column caption="a" name="Calculation_123456" type="real">
          |           </column>
          |           <column-instance derivation="Sum" column="[b]">
          |           </column-instance>
          |           <column-instance derivation="User" column="Calculation_123456" type="real">
          |           </column-instance>
          |           <column-instance derivation="User" column="Calculation_234567" type="string">
          |           </column-instance>
          |           <column-instance derivation="User" column="Calculation_345678" type="integer">
          |           </column-instance>
          |         </datasource-dependencies>
          |       </view>
          |      </table>
          |    </worksheet>
          |  </worksheets>
          |</workbook>
        """.stripMargin)
      val parsed = WorksheetExtractor("testfile.twb", "test", "testfile.twb", xml)
      parsed.toMaps should contain only(
        Map("name" -> "tbl_a",
            "class" -> "TableauDatasource",
            "links" -> List("testfile.twb/sheet1"),
            "src_files" -> List("testfile.twb")),
        Map("name" -> "tbl_c",
            "class" -> "TableauDatasource",
            "links" -> List("testfile.twb/sheet1"),
            "src_files" -> List("testfile.twb")),
        Map("name" -> "testfile.twb/sheet1",
            "class" -> "TableauWorksheet",
            "vgs" -> List("test/testfile.twb"),
            "links" -> List(),
            "columns" -> Map("a" -> Map("type" -> "real"),
                             "Playtime" -> Map("type" -> "string"),
                             "Calculation_345678" -> Map("type" -> "integer")),
            "src_files" -> List("testfile.twb"))
        )
    }

    "not parse empty worksheet" in {
      val xml = XML.loadString(
        """<workbook>
          |  <datasources>
          |   <datasource name="Parameters">
          |   </datasource>
          |   <datasource name="datasource">
          |     <connection>
          |       <relation type="text">
          |         Select a from tbl_a
          |       </relation>
          |       <relation type="table" name="sqlproxy" table="tbl_b">
          |       </relation>
          |       <relation type="table" name="non-sqlproxy" table="tbl_c">
          |       </relation>
          |       <relation type="unknown">
          |       </relation>
          |       <metadata-records>
          |         <metadata-record class="capacity">
          |         </metadata-record>
          |         <metadata-record class="measure">
          |           <remote-name>Calculation_234567</remote-name>
          |           <remote-type>-1</remote-type>
          |           <local-name>[c]</local-name>
          |           <caption>Playtime</caption>
          |           <remote-alias>a</remote-alias>
          |           <local-type>integer</local-type>
          |           <contains-null>true</contains-null>
          |         </metadata-record>
          |       </metadata-records>
          |     </connection>
          |   </datasource>
          |  </datasources>
          |  <worksheets>
          |    <worksheet name="sheet1">
          |      <table>
          |       <view>
          |         <datasources>
          |           <datasource name="Parameters"></datasource>
          |         </datasources>
          |         <datasource-dependencies>
          |           <column caption="a" name="Calculation_123456" type="real">
          |           </column>
          |           <column-instance derivation="Sum" column="[b]">
          |           </column-instance>
          |           <column-instance derivation="User" column="Calculation_123456" type="real">
          |           </column-instance>
          |           <column-instance derivation="User" column="Calculation_234567" type="integer">
          |           </column-instance>
          |         </datasource-dependencies>
          |       </view>
          |      </table>
          |    </worksheet>
          |    <worksheet name="sheet2">
          |      <table>
          |       <view>
          |         <datasources>
          |           <datasource name="Parameters"></datasource>
          |           <datasource caption="datasource" name="xyz"></datasource>
          |         </datasources>
          |         <datasource-dependencies>
          |           <column caption="a" name="Calculation_123456" type="real">
          |           </column>
          |           <column-instance derivation="Sum" column="[b]">
          |           </column-instance>
          |           <column-instance derivation="User" column="Calculation_123456" type="real">
          |           </column-instance>
          |           <column-instance derivation="User" column="Calculation_234567" type="integer">
          |           </column-instance>
          |         </datasource-dependencies>
          |       </view>
          |      </table>
          |    </worksheet>
          |  </worksheets>
          |</workbook>
        """.stripMargin)
      val parsed = WorksheetExtractor("testfile.twb", "test", "testfile.twb", xml)
      parsed.toMaps should contain only(
        Map("name" -> "tbl_a",
            "class" -> "TableauDatasource",
            "links" -> List("testfile.twb/sheet2"),
            "src_files" -> List("testfile.twb")),
        Map("name" -> "tbl_c",
            "class" -> "TableauDatasource",
            "links" -> List("testfile.twb/sheet2"),
            "src_files" -> List("testfile.twb")),
        Map("name" -> "testfile.twb/sheet2",
            "class" -> "TableauWorksheet",
            "vgs" -> List("test/testfile.twb"),
            "links" -> List(),
            "columns" -> Map("a" -> Map("type" -> "real"),
            "Playtime" -> Map("type" -> "integer")),
            "src_files" -> List("testfile.twb"))
        )
    }
  }

}
