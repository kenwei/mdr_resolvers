/**
 * Created by ychen on 7/23/15.
 */
package com.machinezone.resolver

import akka.actor.{ActorRef, Props}
import com.machinezone.resolver.tableau.{DatasourceExtractor, TableauContentExtractor, WorksheetExtractor}
import com.machinezone.util.GraphUtil._
import com.machinezone.util.Helper
import com.machinezone.util.XmlUtil._

/**
 * Resolver of Tableau documents
 */
class TableauDependencyResolver extends RepositoryResolver {

  def resolveDocumentType(filename:String) = {
    filename.split("/").takeRight(3)(0)
  }

  override def getDocumentResolver(filename: String): ActorRef = {
    resolveDocumentType(filename) match {
      case "extract" => context.actorOf(Props[DataSourceWorkbookResolver])
      case "workbook" => context.actorOf(Props[ReportWorkbookResolver])
      case _ => {
        log.warning(s"Found unresolvable file -> $filename")
        context.actorOf(Props.empty)
      }
    }
  }
}

/**
 * Resolver of Tableau extract
 */
class DataSourceWorkbookResolver extends TableauDocumentResolver {

  override val extractor:TableauContentExtractor = DatasourceExtractor

}

/**
 * Resolver of Tableau workbook
 */
class ReportWorkbookResolver extends TableauDocumentResolver {

  override val extractor:TableauContentExtractor = WorksheetExtractor

}

abstract class TableauDocumentResolver extends DocumentResolver {

  override val resolverType = "tableau"

  val extractor:TableauContentExtractor

  override def resolve(repo: String, filename: String): Iterable[Map[String, Any]] = {
    val (prjName, twbName, xml) = filename.loadAsXml
    extractor(Helper.getRelativePath(filename, repo, true), prjName, twbName, xml) toMaps
  }

}
