package com.machinezone.resolver.tableau

import com.machinezone.util.GraphUtil._
import com.machinezone.util.Helper
import com.machinezone.util.XmlUtil._
import com.tinkerpop.blueprints.Direction._
import com.tinkerpop.blueprints.impls.tg.TinkerGraph
import net.sf.jsqlparser.parser.CCJSqlParserUtil
import net.sf.jsqlparser.util.TablesNamesFinder
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._
import scala.xml.{Elem, NodeSeq}

/**
 * Created by kchen on 9/18/15.
 */
trait TableauContentExtractor {

  protected val log = LoggerFactory.getLogger(getClass)

  def apply(filename: String, prjName: String, twbName: String, xml: Elem): TinkerGraph = ???

}

object DatasourceExtractor extends TableauContentExtractor {

  override def apply(filename: String, prjName: String, twbName: String, xml: Elem): TinkerGraph = {
    lazy val finder: TablesNamesFinder = new TablesNamesFinder()
    (xml \ "datasources" \ "datasource")
      .filter(_ ~\@("caption", "name") != "Parameters").foldLeft(new TinkerGraph()) {
      (graph, datasource) =>
        val dsName = datasource ~\@("caption", "name")
        val dsVertex = graph getOrCreateDataSource(s"$prjName/$dsName", filename)

        datasource \ "connection" \ "relation" foreach { relation =>
          relation \@ "type" match {
            case "text" => {
              val normalized = relation.text.replaceAll(">>", ">").replaceAll("<<", "<").stripMargin
              try {
                finder.getTableList(CCJSqlParserUtil.parse(normalized)) foreach { table =>
                  graph getOrCreateTable(table, filename) andLinkTo dsVertex
                }
              }
              catch {
                case e: Exception =>
                  log.warn(s"Unable to resolve $dsName", e)
                  log.debug(s" ==> Normalized source :\n$normalized\n")
                  handleParsingError(twbName, dsName, normalized)
              }
            }
            case "table" if relation \@ "name" != "sqlproxy" => {
              relation \@ "table" match {
                case table if table.nonEmpty =>
                  graph getOrCreateTable(table, filename) andLinkTo dsVertex
              }
            }
            case _ => log.debug("ignore")
          }
        }

        dsVertex.setProperty("columns",
          resolveDatasourceColumnInfo(datasource \ "connection" \ "metadata-records" \ "metadata-record"))

        graph
    }
  }

  def handleParsingError(twbName: String, dsName: String, normalized: String): Unit = {
    Helper.writeFile(s"exception/$twbName.$dsName.sql", normalized)
  }

  def resolveDatasourceColumnInfo(metaRecords: NodeSeq) = {
    metaRecords.filter(_ \@ "class" != "capacity").foldLeft(Map[String, Map[String, String]]()) {
      (columns, metadata) =>
        val cname = metadata ~\>>("caption", "local-name")
        columns + (cname -> Map("type" -> metadata \>> "local-type"))
    }
  }

}

object WorksheetExtractor extends TableauContentExtractor {

  override def apply(filename: String, prjName: String, twbName: String, xml: Elem): TinkerGraph = {
    lazy val lives = DatasourceExtractor(filename, prjName, twbName, xml)
    lazy val dsColumns = xml \ "datasources" \ "datasource" \ "connection" \ "metadata-records" \ "metadata-record"

    (xml \ "worksheets" \ "worksheet").foldLeft(new TinkerGraph()) {
      (graph, worksheet) =>
        val sheetName = worksheet \@ "name"
        val tableView = worksheet \ "table" \ "view"
        val worksheetDatasources = (tableView \ "datasources" \ "datasource").filter(_ ~\@("caption", "name") != "Parameters")

        if (!worksheetDatasources.isEmpty) {
          val sheetVertex = graph.getOrCreateSheet(s"$twbName/$sheetName", filename)
          sheetVertex.setProperty("vgs", List(s"$prjName/$twbName"))
          worksheetDatasources.foreach { datasource =>
            val dsName = datasource ~\@("caption", "name")
            val links = for (edge <- lives.getVertex(s"$prjName/$dsName").getEdges(IN)) yield {
              graph getOrCreateDataSource(edge.getVertex(OUT).getId.toString, filename) andLinkTo sheetVertex
              edge.getVertex(IN).getId
            }
            if (links.isEmpty) {
              graph getOrCreateDataSource(s"$prjName/$dsName", filename) andLinkTo sheetVertex
            }
          }

          sheetVertex.setProperty("columns",
            resolveWorksheetColumnInfo(
              tableView \ "datasource-dependencies" \ "column",
              tableView \ "datasource-dependencies" \ "column-instance",
              dsColumns)
          )
        } else {
          log.info(s"Ignore $twbName/$sheetName, since it depends on nothing...")
        }
        graph
    }
  }

  def resolveWorksheetColumnInfo(usedColumns: NodeSeq, calColumns: NodeSeq, dsColumns: NodeSeq) = {
    val columns = usedColumns.foldLeft(Map[String, Map[String, String]]()) {
      (columns, column) =>
        val cname = column ~\@("caption", "name")
        columns + (cname -> Map("type" -> column \@ "datatype"))
    }
    calColumns.filter(n => !(n \@ "derivation").equalsIgnoreCase("Sum")).foldLeft(columns) {
      (columns, column) =>
        val cname = column ~\@("column", "name") match {
          case calculation if calculation.startsWith("Calculation_") => {
            usedColumns.collectFirst {
              case node if ((node \@ "name").normalized == calculation) => node \@ "caption"
            }.getOrElse(
              dsColumns.collectFirst {
                case node if (node \>> "remote-name") == calculation => node ~\>> ("caption", "local-name")
              }.getOrElse(calculation)
            )
          }
          case name => name
        }
        columns + (cname -> Map("type" -> column \@ "type"))
    }
  }

}

