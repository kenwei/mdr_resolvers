package com.machinezone.util

import scala.xml.{XML, NodeSeq}

/**
 * Created by kchen on 9/15/15.
 */
object XmlUtil {

  implicit class StringWrapper(val s: String) {

    def normalized = {
      s.replaceAll("\\[", "").replaceAll("\\]", "")
    }

    def loadAsXml = {
      val meta = s.split("/").takeRight(2)
      (meta(0), meta(1), XML.loadFile(s))
    }

  }

  implicit class NodeSeqWrapper(val node: NodeSeq) {
    /**
     * Short cut for retrieving normalized text
     */
    def ntext: String = {
      node.text.normalized
    }

    /**
     * Projection that or other
     */
    def ~\(that: String, other: String): NodeSeq = {
      node \ that match {
        case NodeSeq.Empty => node \ other
        case r => r
      }
    }

    /**
     * Attribute that or other
     */
    def ~\@(that: String, other: String): String = {
      node \@ that match {
        case t if t.nonEmpty => t.normalized
        case _ => (node \@ other).normalized
      }
    }

    /**
     * Projection and get normalized text
     */
    def \>>(that: String): String = {
      node \ that ntext
    }

    /**
     * Projection that or other, and then get normalized text
     */
    def ~\>>(that: String, other: String): String = {
      node ~\ (that, other) ntext
    }
  }

}
