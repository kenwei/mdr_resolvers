package com.machinezone.util

import com.machinezone.resolver.DataClass._
import com.machinezone.util.XmlUtil.StringWrapper
import com.tinkerpop.blueprints.Direction._
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.impls.tg.TinkerGraph

import scala.collection.JavaConversions._

/**
 * Created by kchen on 9/17/15.
 */
object GraphUtil {

  implicit class GraphWrapper(graph: TinkerGraph) {

    def getOrCreate(name: String, cls: String, filename: String) = {
      try {
        val v = graph.addVertex(name)
        v.setProperty("name", name)
        v.setProperty("class", cls)
        v.setProperty("src_files", List(filename))
        v
      } catch {
        case _: Throwable => graph.getVertex(name)
      }
    }

    def ++(id: String, cls: String, filename: String) = getOrCreate(id, cls, filename)

    def getOrCreateTable(id: String, filename: String) = getOrCreate(id.normalized.toLowerCase, VerticaTable, filename)

    def getOrCreateSheet(id: String, filename: String) = getOrCreate(id, TableauWorksheet, filename)

    def getOrCreateDataSource(id: String, filename: String) = getOrCreate(id, TableauDatasource, filename)

    def toMaps = {
      graph.getVertices.map { vertex =>
        vertex.setProperty("links", vertex.getEdges(OUT).map { edge =>
          edge.getVertex(IN).getId
        })
        vertex.getPropertyKeys.map(key => key -> vertex.getProperty(key)) toMap
      }
    }

  }

  implicit class VertexWrapper(vertex: Vertex) {

    def andLinkTo(other: Vertex) = {
      vertex.addEdge("link", other)
    }

    def ->(other: Vertex) = andLinkTo(other)

  }

}
