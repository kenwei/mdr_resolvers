package com.machinezone.resolver

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import better.files.File
import com.machinezone.util.Helper.BetterFileWrapper
import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigValueFactory._
import org.scalatest._

import scala.collection.JavaConversions._

/**
 * Created by kchen on 8/31/15.
 */
trait TestFiles {

  val testDeps = File.newTempDir("test_deps")
  val testRepo = (testDeps / "test_repo").createDirectory()
  val tempFile = File.newTemp("test", "resolver")
  val testFile = tempFile.moveTo(testRepo / tempFile.name)

}

class ResolverSpec extends TestKit(ActorSystem("testSystem"))
  with ImplicitSender with WordSpecLike with Matchers with TestFiles {

  class TestableDocumentResolver extends DocumentResolver {

    override protected val resolverType: String = "test"

    override def resolve(repo: String, filename: String): Iterable[Map[String, Any]] =
      List(Map("repo" -> repo,
               "file" -> filename))
  }

  class TestableRepositoryResolver extends RepositoryResolver {

    override protected lazy val generalResolvers = Seq()

    override def getDocumentResolver(filename: String): ActorRef =
      TestActorRef(new TestableDocumentResolver)

  }

  class TestableExceptionDocumentResolver extends DocumentResolver {

    override protected val resolverType: String = "exception"

    override def resolve(repo: String, filename: String): Iterable[Map[String, Any]] =
      throw new Exception("Exception occurred!")
  }

  class TestableExceptionRepositoryResolver extends RepositoryResolver {

    override protected lazy val generalResolvers = Seq()

    override def getDocumentResolver(filename: String): ActorRef =
      TestActorRef(new TestableExceptionDocumentResolver)

  }

  "Repository Resolver" should {

    val config = ConfigFactory.empty().withValue("repo", fromAnyRef(testRepo.name))
                                      .withValue("files", fromIterable(List(testFile.toPath)))

    "handle message as expected" in {
      val repoResolver: TestActorRef[TestableRepositoryResolver] = TestActorRef(new TestableRepositoryResolver)
      repoResolver ! StartResolveRepoMsg(config, testDeps.toAbsolutePath, testRepo.name)
      expectMsg(RepoResolvedMsg())
    }

    "handle message well even exception thrown by DocumentResolver" in {
      val repoResolver: TestActorRef[TestableExceptionRepositoryResolver] = TestActorRef(new TestableExceptionRepositoryResolver)
      repoResolver ! StartResolveRepoMsg(config, testDeps.toAbsolutePath, testRepo.name)
      expectMsg(RepoResolvedMsg())
    }

  }

  "Document Resolver" should {

    "handle message as expected" in {
      val documentResolver = TestActorRef(new TestableDocumentResolver)
      documentResolver ! StartResolveDocumentMsg(testRepo.toAbsolutePath,
                                                 testFile.toAbsolutePath,
                                                 testRepo.toAbsolutePath)
      expectMsg(DocumentProcessedMsg("test", testFile.toAbsolutePath))
    }

  }

}

class ResolverSuits extends Suites(new ResolverSpec) with TestFiles with BeforeAndAfterAll {
  override def afterAll() {
    testDeps.delete(ignoreIOExceptions = true)
  }
}
