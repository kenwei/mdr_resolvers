/**
 * Created by ychen on 8/6/15.
 */

package com.machinezone.resolver.git

import java.io.File

import com.machinezone.resolver.DocumentResolver
import com.machinezone.util.Helper
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.storage.file.FileRepositoryBuilder

import scala.collection.JavaConversions._

class GitResolver extends DocumentResolver {

  override val resolverType = "git"

  override def resolve(repo: String, filename: String): Iterable[Map[String, Any]] = {
    val builder = new FileRepositoryBuilder()
    val repository = builder.setGitDir(new File(s"$repo.git"))
                            .readEnvironment()
                            .findGitDir()
                            .build()
    val git = new Git(repository)
    val srcFile = Helper.getRelativePath(filename, repo, false)
    val lastCommit = git.log().addPath(srcFile).setMaxCount(1).call.head
    List(Map(
      "class" -> "SourceFile",
      "src_repo" -> repository.getConfig.getString("remote", "origin", "url"),
      "src_file" -> Helper.getRelativePath(filename, repo, true),
      "last_commit_hash" -> lastCommit.getName,
      "last_commit_author" -> lastCommit.getAuthorIdent.getName,
      "last_commit_date" -> lastCommit.getAuthorIdent.getWhen.toString,
      "last_commit_comment" -> lastCommit.getFullMessage
    ))
  }

}