/**
 * Created by ychen on 7/22/15.
 */

package com.machinezone.resolver

import java.io.File.{separator => FS}
import java.util.concurrent.atomic.AtomicInteger

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import better.files.File
import com.codahale.jerkson.Json._
import com.machinezone.resolver.git.GitResolver
import com.machinezone.util._
import com.machinezone.util.Helper.BetterFileWrapper
import com.typesafe.config.Config

import scala.collection.JavaConversions._
import scala.collection.mutable.{Map => mMap}

/**
 * The super class of all document resolvers.
 * The major responsibility of a document resolver is to resolve the given document.
 */
abstract class DocumentResolver extends Actor with ActorLogging {

  /** The type of the resolver, default in `unknown` */
  protected val resolverType = "unknown"

  def resolve(repo: String, filename: String): Iterable[Map[String, Any]] = ???

  def getOutputFilename(repo: String, outputDir: String, inputFilename: String): String =
    s"$outputDir/$resolverType/${Helper.getRelativePath(inputFilename, repo, true).replaceAll("/", "_")}.json"

  def dumpToJson(filename: String, vertices: Iterable[Map[String, Any]]): Unit = {
    try {
      Helper.writeFile(filename, generate(vertices))
    } catch {
      case e: Exception =>
        log.warning("Unable to converting object to json", e)
    }
  }

  override def receive: Receive = {
    case StartResolveDocumentMsg(repo, filename, outputDir) =>
      try {
        val data = resolve(repo, filename)
        dumpToJson(getOutputFilename(repo, outputDir, filename), data)
        sender ! DocumentProcessedMsg(resolverType, filename)
      } catch {
        case e: Exception =>
          log.error(e, s"Exception occurred during the handling of $filename")
          sender ! DocumentProcessFailedMsg(resolverType, filename, e)
      }
  }
}

/**
 * The super class of all repository resolvers. The major responsibilities of a repository resolver are -
 *
 * 1) To traverse the interested files within the repo and launch the corresponding document resolvers to parse the file.
 * 1.1) By default, the built-in Git Resolver will automatically launched to resolve the git commit info of traversed source files.
 *      Subclasses of RepoResolver doesn't have to do this.
 * 2) Once all the files have been resolved, notify the sender with an repository resolved message.
 *
 */
abstract class RepositoryResolver extends Actor with ActorLogging {

  protected var totalDocuments = 0

  /** TODO: should be configurable rather than hardcoded */
  protected lazy val generalResolvers = Seq(context.actorOf(Props[GitResolver]))
  private val documentsProcessed = mMap[String, AtomicInteger]()

  private var trigger: Option[ActorRef] = None

  def getDocumentResolver(filename: String): ActorRef = ???

  def resolveDocument(repo: String, fpath: String, outputDir: String) = {
    (generalResolvers :+ getDocumentResolver(fpath)) foreach {
      _ ! StartResolveDocumentMsg(repo, fpath, outputDir)
    }
  }

  def resolveRepository(config: Config, deps: String, output: String) = {
    val repo = List(deps, config.getString("repo")).mkString(FS)
    val files = config.getStringList("files")
    val repoDir = File(repo).selfOfSymTarget
    val processingFiles = files.flatMap (repoDir.glob(_))
    if (processingFiles.nonEmpty) {
      totalDocuments = processingFiles.foldLeft(0) { (s, fpath) =>
        resolveDocument(repoDir.toAbsolutePath + FS,
                        fpath.toAbsolutePath,
                        output)
        s + 1
      }
    } else {
      log.warning(s"Nothing found with $repo and $files, please check the config.")
      trigger.map(_ ! RepoEmptyMsg())
    }
  }

  def receive = {
    case StartResolveRepoMsg(config, deps, output) => {
      trigger = Some(sender)
      resolveRepository(config, deps, output)
    }
    case DocumentProcessedMsg(resolverType, filename) => countProcessedDocument(resolverType)
    case DocumentProcessFailedMsg(resolverType, filename, e) => countProcessedDocument(resolverType)
    case x => log.warning(s"Oops! Unrecognized message $x from $sender! ")
  }

  protected def countProcessedDocument(resolverType: String) = {
    val count: Int = documentsProcessed.getOrElseUpdate(resolverType, new AtomicInteger(0)).incrementAndGet()
    log.info(s"... $resolverType document processed $count of $totalDocuments")

    if (documentsProcessed.values.filter(_.get() == totalDocuments).size == documentsProcessed.size) {
      log.info(s"All $totalDocuments documents have been processed, finishing up the resolution request.")
      trigger.map(_ ! RepoResolvedMsg())
    }

  }
}
