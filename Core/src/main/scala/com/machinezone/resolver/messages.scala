package com.machinezone.resolver

import com.typesafe.config.Config

/**
 * Created by kchen on 8/31/15.
 */
sealed trait ResolverMessage

case class StartResolveRepoMsg(config: Config, deps: String, outputDir: String) extends ResolverMessage

case class StartResolveDocumentMsg(repo: String, filename: String, outputDir: String) extends ResolverMessage

case class DocumentProcessedMsg(resolverType: String, filename: String) extends ResolverMessage

case class DocumentProcessFailedMsg(resolverType: String, filename: String, e: Exception) extends ResolverMessage

case class RepoRefreshedMsg(config: Config) extends ResolverMessage

case class RepoResolvedMsg() extends ResolverMessage

case class RepoEmptyMsg() extends ResolverMessage
