package com.machinezone.resolver

/**
 * Created by kchen on 8/31/15.
 */
object DataClass extends Enumeration {
  type DataClass = Value
  val TableauWorksheet = "TableauWorksheet"
  val TableauDatasource = "TableauDatasource"
  val VerticaTable = "VerticaTable"
  val DatameerImportJob = "DatameerImportJob"
  val DatameerSheet = "DatameerSheet"
  val DatameerExportJob = "DatameerExportJob"
}
