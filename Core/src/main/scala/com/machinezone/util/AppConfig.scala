package com.machinezone.util

import com.typesafe.config.ConfigFactory

object AppConfig {
  val env = sys.env.getOrElse("APP_ENV", "dev")
  val conf = ConfigFactory.load()

  def apply() = conf.getConfig(env).withFallback(conf.getConfig("common"))
}
