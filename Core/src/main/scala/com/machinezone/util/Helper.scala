/**
 * Created by ychen on 7/30/15.
 */

package com.machinezone.util

import java.io.{BufferedWriter, File, FileWriter}
import better.files.{File => BetterFile}

object Helper {

  def writeFile(filename: String, content: String): Unit = {
    val bw = new BufferedWriter(new FileWriter(new File(filename)))
    bw.write(content)
    bw.close()
  }

  def createOutputDirectory(outputDirectory: String): Unit = {
    val output = new File(outputDirectory)
    if (!output.exists()) {
      output.mkdir()
    }
  }

  def getRelativePath(filename: String, folder: String, include: Boolean): String =
    if (include) filename.replaceAll(folder, folder.split("/").takeRight(1)(0) + "/")
    else filename.replaceAll(folder, "")

  implicit class BetterFileWrapper(file:BetterFile) {
    def selfOfSymTarget = file.symLink match {
      case Some(s) => s
      case _ => file
    }
    def toPath = file.path.toString
    def toAbsolutePath = file.path.toAbsolutePath.toString
  }

}
