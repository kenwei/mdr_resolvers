package com.machinezone.resolver

import java.io.File
import java.util.Properties

import akka.actor.{ActorRef, Props}
import com.codahale.jerkson.Json._
import com.typesafe.config.Config
import org.python.util.PythonInterpreter

import scala.collection.JavaConversions._

class VerticaResolver extends RepositoryResolver {

  override def getDocumentResolver(filename: String): ActorRef = context.actorOf(Props.empty)

  override def resolveRepository(config: Config, deps: String, output: String) = {
    log.info("Starting to resolve repository..")

    // Notes: Store current user directory for switching back later.
    val userDir = System.getProperty("user.dir")

    // TODO: Set `name` from config or parameter instead of hardcode.
    val pythonConfig = Map("name" -> "vertica",
      "output" -> new File(output).getAbsolutePath,
      "files" -> config.getStringList("files").map(_.replace("**/", "")),
      "path" -> new File(deps).getAbsolutePath,
      "repo" -> config.getString("repo"))
    try {
      log.info("Starting to initiate mdr_vertica..")
      val props: Properties = new Properties()
      props.setProperty("user.dir", new File(config.getString("user.dir")).getAbsolutePath)
      props.setProperty("python.path",
        config.getStringList("python.path").map(new File(_).getAbsolutePath).mkString(":"))
      PythonInterpreter.initialize(System.getProperties, props, Array(""))
      val interpreter = new PythonInterpreter()
      log.info("Interpreter initialized..")
      interpreter.exec("from analyzer.analyze import analyze_all")
      interpreter.set("config", generate(pythonConfig))
      interpreter.exec("analyze_all(config)")
    } catch {
      case e: Exception => log.error(e, "VerticaResolver failed:")
    } finally {
      System.setProperty("user.dir", userDir)
    }

    // NOTE: leave messages handling to base repository resolver
    super.resolveRepository(config, deps, output)
  }
}
