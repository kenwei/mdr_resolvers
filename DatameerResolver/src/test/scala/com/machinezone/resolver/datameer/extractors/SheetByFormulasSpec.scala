/**
 * Created by bshih on 9/7/15.
 */

package com.machinezone.resolver.datameer.extractors


import com.machinezone.resolver.datameer.utils.JsonWrapper
import org.json4s.JsonAST._
import org.scalatest.{Entry, Inspectors, Matchers, WordSpec}

import scala.collection.JavaConversions.mapAsJavaMap


trait DummyFormulaBasedSheet extends JsonWrapper {

  val sheetName: String = "sheet_name"
  val columnName1: String = "col_name_1"
  val columnName2: String = "col_name_2"
  val referenceSheetName1: String = "ref_sheet_name_1"
  val referenceSheetName2: String = "ref_sheet_name_2"
  val jsonFilePath: String = "/this/is/a/path/file.json"
  val workbookName: String = "workbook_name.wbk"
  val dummySheetJsonString: String =
    s"""
       |{
       |  "name": "$sheetName",
       |  "formulas": [
       |    {
       |      "columnName": "$columnName1",
       |      "formulaString": "=IF(#$referenceSheetName1!$columnName1== 1; INT(JSON_VALUE(#$referenceSheetName1!$columnName2; \\\"jsonProperty1\\\"));null)"
       |    },
       |    {
       |      "columnName": "$columnName2",
       |      "formulaString": "=IF(#$referenceSheetName2!$columnName1 == 1; INT(JSON_VALUE(#$referenceSheetName2!$columnName2; 'jsonProperty2'));null)"
       |    }
       |  ],
       |  "columnStyles": [
       |    {
       |      "columnId": "0",
       |      "name": "$columnName1",
       |      "width": 127,
       |      "thousandSeparator": true
       |    },
       |    {
       |      "columnId": "1",
       |      "name": "$columnName2",
       |      "width": 127,
       |      "thousandSeparator": false
       |    }
       |  ],
       |
       |  "repo": "repo_name",
       |  "jsonFilePath": "$jsonFilePath",
       |  "thisWorkbookFullName": "$workbookName"
       |}
    """.stripMargin

  val deserializedDummySheet: JValue = super.deserialize(dummySheetJsonString)
}

class FormulasSpec extends WordSpec
with Matchers with Inspectors
with DummyFormulaBasedSheet with Formula {

  val parsed: Map[String, List[String]] =
    getSheetsColumns(deserializedDummySheet)

  "Formulas" should {
    "be parsed" in {

      mapAsJavaMap(parsed) should contain allOf(
        Entry(referenceSheetName1, List(columnName1, columnName2)),
        Entry(referenceSheetName2, List(columnName1, columnName2))
        )
    }
  }
}

class SheetByFormulasSpec extends WordSpec
with Matchers with Inspectors with DummyFormulaBasedSheet with JsonWrapper {

  val processed: JValue = super.deserialize(super.serialize(
    deserializedDummySheet match {
      case SheetByFormulas(sheetByFormulasMatched) => sheetByFormulasMatched
    }
  ))

  "A sheet by formulas" should {
    "be parsed" in {

      forAll(processed.children) {
        eachVertex => {
          (eachVertex \ "name": String) should startWith(s"ody/$workbookName")
          (eachVertex \ "class": String) shouldBe ("DatameerSheet")
          ((eachVertex \ "src_files")(0): String) shouldBe (jsonFilePath)
          ((eachVertex \ "vgs")(0):String) shouldBe (s"ody/$workbookName")

          (for (eachDownStreamV <- (eachVertex \ "links").children) yield {
            eachDownStreamV: String
          }) should (be(empty) or contain(s"ody/$workbookName/$sheetName"))

          forAll(List(columnName1, columnName2)) { columnName =>
            (eachVertex \ "columns" \ columnName) shouldBe a[JObject]
          }
        }
      }
    }
  }
}
