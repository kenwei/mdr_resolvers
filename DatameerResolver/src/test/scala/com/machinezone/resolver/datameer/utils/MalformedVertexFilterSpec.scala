
/**
 * Created by bshih on 9/4/15.
 */

package com.machinezone.resolver.datameer.utils

import org.scalatest.{Matchers, WordSpec}


class MalformedVertexFilterSpec extends WordSpec with Matchers {

  "A MalformedVertexFilter" should {
    "filter inappropriate vertices" in {

      val testVertices: List[Map[String, Any]] = List(
        Map(
          "name" -> "gamem/vertex.wbk/sheet",
          "links" -> List(
            "game/export.csv",
            "game/another.csv"
          )
        ),
        Map(
          "name" -> "inappropriate//vertex",
          "links" -> List(
            "game/another.csv"
          )
        ),
        Map(
          "name" -> "inappropriate/in.links/list",
          "links" -> List(
            "inappropriate/.csv"
          )
        )
      )

      testVertices.filter(MalformedVertexFilter(_)).count(x => true) shouldBe 1
    }
  }
}