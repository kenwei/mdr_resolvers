/**
 * Created by bshih on 9/7/15.
 */

package com.machinezone.resolver.datameer.extractors

import com.machinezone.resolver.datameer.utils.JsonWrapper
import org.scalatest.{Inspectors, Matchers, WordSpec}


class SheetFromDatasourceSpec extends WordSpec
with Matchers with Inspectors with JsonWrapper {

  val sheetName: String = "sheet_name"
  val jsonFilePath: String = "/this/is/a/path/file.json"
  val workbookName: String = "workbook_name.wbk"
  val dummySheetJsonString: String =
    s"""
      |{
      |  "name": "$sheetName",
      |  "filterSourceSheet": "source_sheet_name",
      |  "filterTargetSheet": "target_sheet_name",
      |  "filterArguments": [
      |    {
      |      "filterExpression": "FORMULA",
      |      "value": "(#col_name\u003d\u003d5566 || #col_name\u003d\u003d5566) \u0026\u0026 #col_name \u003e\u003d ROUNDTIME(NOW()-12h; ) \u0026\u0026 #col_name\u003d\u003d69",
      |      "valueStatic": false,
      |      "type": "ADVANCED_FORMULA"
      |    }
      |  ],
      |  "datasource": {
      |    "path": "whatever/the/path/is.dummy"
      |  },
      |  "columnStyles": [],
      |
      |  "repo": "repo_name",
      |  "jsonFilePath": "$jsonFilePath",
      |  "thisWorkbookFullName": "$workbookName"
      |}
    """.stripMargin

  "A sheet from datasource" should {
    "be parsed" in {

      forAll(super.deserialize(dummySheetJsonString) match {
        case SheetFromDatasource(sheetFromDatasourceMatched) => sheetFromDatasourceMatched
      }) { eachVertex =>
        forAll(eachVertex) {
          eachTerm => eachTerm match {
            case (k, v) => k match {
              case "name" =>
                v.toString should not be empty
                v shouldBe a[String]
              case "class" => v.toString should (
                be("DatameerImportJob") or
                be("DatameerSheet")
              )
              case "vgs" => v shouldBe List(s"ody/$workbookName")
              case "links" => v should (
                be(List()) or
                be(List(s"ody/$workbookName/$sheetName"))
              )
              case "src_files" => v shouldBe Some(List(jsonFilePath))
              case "columns" => v shouldBe Some(Map())
            }
          }
        }
      }
    }
  }
}
