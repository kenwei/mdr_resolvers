/**
 * Created by bshih on 9/7/15.
 */

package com.machinezone.resolver.datameer.extractors

import com.machinezone.resolver.datameer.utils.JsonWrapper
import org.scalatest.{Inspectors, Matchers, WordSpec}


class SheetFromReferencedSheetSpec extends WordSpec
with Matchers with Inspectors with JsonWrapper {

  val sheetName: String = "sheet_name"
  val columnName1: String = "col_name_1"
  val columnName2: String = "col_name_2"
  val jsonFilePath: String = "/this/is/a/path/file.json"
  val workbookName: String = "workbook_name.wbk"
  val dummySheetJsonString: String =
    s"""
      |{
      |  "name": "$sheetName",
      |  "filterSourceSheet": "source_sheet_name",
      |  "filterTargetSheet": "target_sheet_name",
      |  "filterArguments": [
      |    {
      |      "filterExpression": "FORMULA",
      |      "value": "(#col_name\u003d\u003d5566 || #col_name\u003d\u003d5566) \u0026\u0026 #col_name \u003e\u003d ROUNDTIME(NOW()-12h; ) \u0026\u0026 #col_name\u003d\u003d69",
      |      "valueStatic": false,
      |      "type": "ADVANCED_FORMULA"
      |    }
      |  ],
      |  "referencedSheet": {
      |    "name": "referenced_sheet_name",
      |    "workbook": {
      |      "path": "a/dummy/path/with/file.name"
      |    }
      |  },
      |  "columnStyles": [
      |    {
      |      "columnId": "0",
      |      "name": "$columnName1",
      |      "width": 127,
      |      "thousandSeparator": true
      |    },
      |    {
      |      "columnId": "1",
      |      "name": "$columnName2",
      |      "width": 127,
      |      "thousandSeparator": false
      |    }
      |  ],
      |
      |  "repo": "repo_name",
      |  "jsonFilePath": "$jsonFilePath",
      |  "thisWorkbookFullName": "$workbookName"
      |}
    """.stripMargin

  "A sheet from referenced sheet" should {
    "be parsed" in {

      forAll(super.deserialize(dummySheetJsonString) match {
        case SheetFromReferencedSheet(sheetFromReferencedSheetMatched) => sheetFromReferencedSheetMatched
      }) { eachVertex =>
        forAll(eachVertex) {
          eachTerm => eachTerm match {
            case (k, v) => k match {
              case "name" =>
                v.toString should not be empty
                v shouldBe a[String]
              case "class" => v.toString shouldBe "DatameerSheet"
              case "vgs" => v shouldBe List(s"ody/$workbookName")
              case "links" => v should (
                be(List()) or
                be(List(s"ody/$workbookName/$sheetName"))
              )
              case "src_files" => v shouldBe Some(List(jsonFilePath))
              case "columns" => v shouldBe Some(Map(
                columnName1 -> Map(),
                columnName2 -> Map()
              ))
            }
          }
        }
      }
    }
  }
}
