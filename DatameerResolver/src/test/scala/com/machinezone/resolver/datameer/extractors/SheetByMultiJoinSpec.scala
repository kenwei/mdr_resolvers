/**
 * Created by bshih on 9/7/15.
 */

package com.machinezone.resolver.datameer.extractors

import com.machinezone.resolver.datameer.utils.JsonWrapper
import org.json4s.JsonAST._
import org.scalatest.{Entry, Inspectors, Matchers, WordSpec}

import scala.collection.JavaConversions.mapAsJavaMap


trait DummyMultiJoinSheet extends JsonWrapper {

  val sheetName: String = "sheet_name"
  val joinSheet1: String = "join_sheet_1"
  val joinSheet2: String = "join_sheet_2"
  val joinSheet3: String = "join_sheet_3"
  val joinColumn1_1: String = "join_column_1_1"
  val joinColumn1_2: String = "join_column_1_2"
  val joinColumn2_1: String = "join_column_2_1"
  val joinColumn2_2: String = "join_column_2_2"
  val joinColumn3_1: String = "join_column_3_1"
  val joinColumn3_2: String = "join_column_3_2"
  val otherColumn1_1: String = "other_column_1_1"
  val otherColumn1_2: String = "other_column_1_2"
  val otherColumn2_1: String = "other_column_2_1"
  val otherColumn2_2: String = "other_column_2_2"
  val otherColumn3_1: String = "other_column_3_1"
  val otherColumn3_2: String = "other_column_3_2"
  val jsonFilePath: String = "/this/is/a/path/file.json"
  val workbookName: String = "workbook_name.wbk"
  val dummySheetJsonString: String =
    s"""
       |{
       |  "name": "$sheetName",
       |  "sheetDefinition": {
       |    "joinCategory": "MULTI_JOIN",
       |    "joinPairs": [
       |      {
       |        "joinType": "OUTER_LEFT",
       |        "sheet1": "$joinSheet1",
       |        "sheet2": "$joinSheet2",
       |        "joinColumns1": [
       |          "$joinColumn1_1",
       |          "$joinColumn1_2"
       |        ],
       |        "joinColumns2": [
       |          "$joinColumn2_1",
       |          "$joinColumn2_2"
       |        ]
       |      },
       |      {
       |        "joinType": "OUTER_LEFT",
       |        "sheet1": "$joinSheet1",
       |        "sheet2": "$joinSheet3",
       |        "joinColumns1": [
       |          "$joinColumn1_1",
       |          "$joinColumn1_2"
       |        ],
       |        "joinColumns2": [
       |          "$joinColumn3_1",
       |          "$joinColumn3_2"
       |        ]
       |      }
       |    ],
       |    "sheetToIncludeColumns": {
       |      "$joinSheet1": [
       |        "$otherColumn1_1",
       |        "$otherColumn1_2"
       |      ],
       |      "$joinSheet2": [
       |        "$otherColumn2_1",
       |        "$otherColumn2_2"
       |      ],
       |      "$joinSheet3": [
       |        "$otherColumn3_1",
       |        "$otherColumn3_2"
       |      ]
       |    }
       |  },
       |  "columnStyles": [],
       |
      |  "repo": "repo_name",
       |  "jsonFilePath": "$jsonFilePath",
       |  "thisWorkbookFullName": "$workbookName"
       |}
    """.stripMargin

  val deserializedDummySheet: JValue = super.deserialize(dummySheetJsonString)
}

class JoinInMultiJoinSpec extends WordSpec
with Matchers with Inspectors
with DummyMultiJoinSheet with Join with JsonWrapper {

  val myOwnColumns: Map[String, Map[Nothing, Nothing]] =
    getSheetsColumnsInMyself(deserializedDummySheet)

  val downStreamColumns: Map[String, List[String]] =
    getDownStreamSheetsColumns(deserializedDummySheet)

  "multi joined columns" should {

    "be parsed for vertex itself" in {

      mapAsJavaMap(myOwnColumns) should contain allOf(
        Entry(s"$joinSheet1.$otherColumn1_1", Map().empty),
        Entry(s"$joinSheet1.$otherColumn1_2", Map().empty),
        Entry(s"$joinSheet2.$otherColumn2_1", Map().empty),
        Entry(s"$joinSheet2.$otherColumn2_2", Map().empty)
        )
    }

    "be parsed for down stream vertices" in {

      mapAsJavaMap(downStreamColumns) should contain allOf(
        Entry(joinSheet1, List(
          joinColumn1_1,
          joinColumn1_2,
          otherColumn1_1,
          otherColumn1_2
        )),
        Entry(joinSheet2, List(
          joinColumn2_1,
          joinColumn2_2,
          otherColumn2_1,
          otherColumn2_2
        ))
        )
    }
  }
}

class SheetByMultiJoinSpec extends WordSpec
with Matchers with Inspectors with JsonWrapper with DummyMultiJoinSheet {

  val processed: JValue = super.deserialize(super.serialize(
    deserializedDummySheet match {
      case SheetByMultiJoin(sheetByMultiJoinMatched) => sheetByMultiJoinMatched
    }
  ))

  "A sheet by multi join" should {
    "be parsed" in {

      forAll(processed.children) {
        eachVertex => {
          (eachVertex \ "name": String) should startWith(s"ody/$workbookName")
          (eachVertex \ "class": String) shouldBe ("DatameerSheet")
          ((eachVertex \ "src_files")(0): String) shouldBe (jsonFilePath)
          (eachVertex \ "columns") shouldBe a[JObject]
          ((eachVertex \ "vgs")(0):String) shouldBe (s"ody/$workbookName")

          (for (eachDownStreamV <- (eachVertex \ "links").children) yield {
            eachDownStreamV: String
          }) should (be(empty) or contain(s"ody/$workbookName/$sheetName"))
        }
      }
    }
  }
}
