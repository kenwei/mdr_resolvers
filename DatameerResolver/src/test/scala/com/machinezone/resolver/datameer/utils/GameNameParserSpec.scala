/**
 * Created by bshih on 9/4/15.
 */

package com.machinezone.resolver.datameer.utils

import org.scalatest.{Matchers, WordSpec}


class GameNameParserSpec extends WordSpec with Matchers {

  "A GameNameParser" should {
    "parse fullFileName as game name" in {
      val dummyWisoPath1: String = "dummy/long/path/wiso_name/path/file.xxx"
      val dummyWisoPath2: String = "dummy/long/path/game_name/path/wiso_file.xxx"
      val dummyOdyPath: String = "dummy/long/path/game_name/path/file.xxx"
      val dummyMalformedPath = "not_a_path"

      GameNameParser(dummyWisoPath1) shouldBe "wiso"
      GameNameParser(dummyWisoPath2) shouldBe "wiso"
      GameNameParser(dummyOdyPath) shouldBe "ody"
      GameNameParser(dummyMalformedPath) shouldBe a [String]
    }
  }
}