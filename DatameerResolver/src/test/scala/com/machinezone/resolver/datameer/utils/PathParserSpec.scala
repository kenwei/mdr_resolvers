
/**
 * Created by bshih on 9/4/15.
 */

package com.machinezone.resolver.datameer.utils

import java.io.{File, FileWriter}
import org.json4s.JsonAST.JValue
import org.scalatest.{Matchers, WordSpec}


class PathParserSpec extends WordSpec with Matchers with JsonWrapper {

  "A PathParser" should {
    val testPath1: String = "/it/is/a/dummy_path/and_dummy_name.dmy"
    val testPath2: String = "/itis/another/dummy_path/and_dummy_name.dmy"
    val testJsonString: String =
      s"""
         |{
         |  "version": "0.0.001",
         |  "className": "className",
         |  "file": {
         |    "path": "$testPath1",
         |    "name": "workbookOrExportJobName"
         |  },
         |  "sheets": [
         |    {
         |      "name": "sheetName",
         |      "sheetType": "aSheet",
         |      "filterArguments": [
         |        {
         |          "column": "col_1",
         |          "value": "-"
         |        }
         |      ],
         |      "datasource": {
         |        "path": "$testPath2"
         |      }
         |    }
         |  ]
         |}
        """.stripMargin

    def getFakeConfigPath(jsonString: String): String = {
      val file = File.createTempFile("test", "path_parser")
      val writer = new FileWriter(file)
      try {
        writer.write(jsonString.stripMargin)
      } finally writer.close()

      file.getAbsolutePath
    }

    val testMaterial: JValue =
      super.deserialize(FileWrapper.read(getFakeConfigPath(testJsonString)))

    "parse file path" in {
      List(
        PathParser(testMaterial \ "file" \ "path")(m => m.filePath),
        PathParser(testMaterial \ "file" \ "path")(m => m.fullFileName)
      ).mkString("") shouldBe testPath1
    }

    "parse datasource of sheet's path" in {
      List(
        PathParser((testMaterial \ "sheets")(0) \ "datasource" \ "path")(m => m.filePath),
        List(
          PathParser((testMaterial \ "sheets")(0) \ "datasource" \ "path")(m => m.fileName),
          PathParser((testMaterial \ "sheets")(0) \ "datasource" \ "path")(m => m.fileType)
        ).mkString(".")
      ).mkString("") shouldBe testPath2
    }
  }
}