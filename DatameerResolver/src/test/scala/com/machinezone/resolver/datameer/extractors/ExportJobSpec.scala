/**
 * Created by bshih on 9/7/15.
 */

package com.machinezone.resolver.datameer.extractors

import com.machinezone.resolver.datameer.utils.JsonWrapper
import org.scalatest.{Inspectors, Matchers, WordSpec}


class ExportJobSpec extends WordSpec
with Matchers with Inspectors with JsonWrapper {

  val exportJobName: String = "export_job_name.exp"
  val exportJobPath: String = s"/whatever/the/path/$exportJobName"
  val sourceWorkbookName: String = "workbook_name.wbk"
  val sourceWorkbookPath: String = s"/a/dummy/path/with/$sourceWorkbookName"
  val columnName1: String = "maybe_a_ts"
  val columnName2: String = "col_name_2"
  val columnName3: String = "col_name_3"
  val jsonFilePath: String = "/this/is/a/path/file.json"
  val dummyExportJobJsonString: String =
    s"""
       |{
       |  "version": "0.0.001",
       |  "file": {
       |    "path": "$exportJobPath",
       |    "description": "",
       |    "name": "workbook_name"
       |  },
       |  "sheet": {
       |    "name": "sheet_name",
       |    "workbook": {
       |      "path": "$sourceWorkbookPath"
       |    }
       |  },
       |  "mappings": [
       |    {
       |      "name": "$columnName1",
       |      "srcColumnIndex": 0,
       |      "nullable": false,
       |      "pattern": "yyyy-MM-dd HH:mm:ss"
       |    },
       |    {
       |      "name": "$columnName2",
       |      "srcColumnIndex": 1,
       |      "nullable": false
       |    },
       |    {
       |      "name": "$columnName3",
       |      "srcColumnIndex": 2,
       |      "nullable": true
       |    }
       |  ],
       |
       |  "repo": "repo_name",
       |  "jsonFilePath": "$jsonFilePath"
       |}
    """.stripMargin

  "An export job" should {
    "be parsed" in {

      forAll(super.deserialize(dummyExportJobJsonString) match {
        case ExportJob(exportJobMatched) => exportJobMatched
      }) { eachVertex =>
        forAll(eachVertex) {
          eachTerm => eachTerm match {
            case (k, v) => k match {
              case "name" =>
                v.toString should not be empty
                v shouldBe a[String]
              case "class" => v.toString should (
                be("DatameerExportJob") or
                be("DatameerSheet")
              )
              case "links" => v should (
                be(List("ody/" + exportJobName.replace(".exp", ".csv"))) or
                be(List("ody/" + exportJobName))
              )
              case "src_files" => v shouldBe Some(List(jsonFilePath))
              case "columns" => v shouldBe Some(Map(
                columnName1 -> Map(),
                columnName2 -> Map(),
                columnName3 -> Map()
              ))
              case "vgs" => v shouldBe (List(s"ody/$sourceWorkbookName"))
            }
          }
        }
      }
    }
  }
}
