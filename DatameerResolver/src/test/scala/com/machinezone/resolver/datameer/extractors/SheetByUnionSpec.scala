/**
 * Created by bshih on 9/7/15.
 */

package com.machinezone.resolver.datameer.extractors

import com.machinezone.resolver.datameer.utils.JsonWrapper
import org.scalatest.{Inspectors, Matchers, WordSpec}


class SheetByUnionSpec extends WordSpec
with Matchers with Inspectors with JsonWrapper {

  val sheetName: String = "sheet_name"
  val jsonFilePath: String = "/this/is/a/path/file.json"
  val workbookName: String = "workbook_name.wbk"
  val dummySheetJsonString: String =
    s"""
      |{
      |  "name": "$sheetName",
      |  "sheetDefinition": {
      |    "unionSources": [
      |      "sheet1",
      |      "sheet2"
      |    ]
      |  },
      |
      |  "repo": "repo_name",
      |  "jsonFilePath": "$jsonFilePath",
      |  "thisWorkbookFullName": "$workbookName"
      |}
    """.stripMargin

  "A sheet by union" should {
    "be parsed" in {

      forAll(super.deserialize(dummySheetJsonString) match {
        case SheetByUnion(sheetByUnionMatched) => sheetByUnionMatched
      }) { eachVertex =>
        forAll(eachVertex) {
          eachTerm => eachTerm match {
            case (k, v) => k match {
              case "name" =>
                v.toString should not be empty
                v shouldBe a[String]
              case "class" => v.toString shouldBe "DatameerSheet"
              case "vgs" => v shouldBe List(s"ody/$workbookName")
              case "links" => v should (
                be(List()) or
                be(List(s"ody/$workbookName/$sheetName"))
              )
              case "src_files" => v shouldBe Some(List(jsonFilePath))
              case "columns" => v shouldBe Some(Map())
            }
          }
        }
      }
    }
  }
}
