/**
 * Created by bshih on 8/11/15.
 */
package com.machinezone.resolver.datameer.extractors

import com.machinezone.resolver.datameer.utils._
import com.machinezone.resolver.{DataClass, DocumentResolver}
import org.json4s.JsonAST._


trait PairListToMap {

  /**
   * An implicated conversion.
   *
   * @param pairList in format: List((key, value), (key,value))
   * @return Map in format: Map(key, List(value, value))
   */

  implicit def transform(pairList: List[(String, String)]): Map[String, List[String]] =
    pairList.groupBy(_._1).map { case (k, v) => (k, v.map(_._2)) }
}

trait Formula extends JsonWrapper with PairListToMap {

  def getSheetsColumns(materialOfSheet: JValue): Map[String, List[String]] = {
    val parsedSheetsColumns: List[List[(String, String)]] =
      for(eachFormula <- (materialOfSheet \ "formulas").children) yield {
        formulaParser(eachFormula \ "formulaString", List())._2
      }
    parsedSheetsColumns.flatten.distinct.reverse
  }

  /**
   * Test if a formula string has anything to extract.
   */
  private val parsablePattern =
    """(?:.*#.*!.*)""".r

  /**
   * What are these regex. for? See test case: SheetByFormulasSpec.dummySheetJsonString.
   */
  private val formulaPatternWithEqualMark =
    """(?:.*#)([_\d\w]*)(?:!)([_\.\d\w]+)(?: *==.*)""".r
  private val formulaPattern = """(?:.*#)([_\d\w]*)(?:!)([_\.\d\w]+)(?:.*)""".r

  /**
   * Extract sheets and columns used in formula recursively.
   *
   * Steps:
   * 1. Every recursive will extract a pair of (sheet, column) and put it into sheetColumnsContain.
   * 2. Then, call stripParsedFormula to delete extracted substring in formula.
   * 3. Recursive 1, 2 until there's nothing to extract in formula
   *
   * @param formula
   * @param sheetColumnsContain: Empty List() to contain result in List((sheet, col), ... ) format
   * @return List((sheet, column), ... )
   */
  private def formulaParser(formula: String, sheetColumnsContain: List[(String, String)]): (String, List[(String, String)]) =
    formula match {
      case parsablePattern() =>

        val (sheet: String, column: String, stripUntil: String) = formula match {
          case formulaPatternWithEqualMark(sheet, column) => (sheet, column, s"#$sheet!$column")
          case formulaPattern(sheet, column) => (sheet, column, s"#$sheet!$column")
          case _ => ("", "", "")
        }

        formulaParser(
          stripParsedFormula(formula, stripUntil),
          (sheet, column) :: sheetColumnsContain
        )
      case _ => (formula, sheetColumnsContain.filter(eachPair => eachPair._1.nonEmpty && eachPair._2.nonEmpty))
    }

  private def stripParsedFormula(formula: String, stringToBeStrip: String): String =
    formula.replaceFirst(stringToBeStrip, "")
}

trait Join extends JsonWrapper with PairListToMap {

  def getSheetsColumnsInMyself(materialOfSheet: JValue): Map[String, Map[Nothing, Nothing]] =
    getSheetsAndColumns(materialOfSheet, isDownStream = false).foldLeft(Map[String, Map[Nothing, Nothing]]())((m, pair) => {
      m + (s"${pair._1}.${pair._2}" -> Map())
    })

  def getDownStreamSheetsColumns(materialOfSheet: JValue): Map[String, List[String]] =
    getSheetsAndColumns(materialOfSheet, isDownStream = true)

  private def getSheetsAndColumns(materialOfSheet: JValue, isDownStream: Boolean = false): List[(String, String)] =
    (materialOfSheet \ "sheetDefinition" \ "joinPairs").children.foldLeft(List[List[(String, String)]]())((l, eachPair) => {

      (for (number <- List(1, 2)) yield {

        val sheetName: String = eachPair \ s"sheet$number"

        val sheetColumnsInSheetToIncludeColumns: List[(String, String)] =
          for (eachOtherColumn <- (materialOfSheet \ "sheetDefinition" \ "sheetToIncludeColumns" \ sheetName).children) yield {
            (sheetName, eachOtherColumn: String)
          }

        val sheetColumnsInJoinPairs: List[(String, String)] =
          for (eachColumn <- (eachPair \ s"joinColumns$number").children) yield {
            (sheetName, eachColumn: String)
          }

        isDownStream match {
          case true => sheetColumnsInJoinPairs ::: sheetColumnsInSheetToIncludeColumns
          case _ => sheetColumnsInSheetToIncludeColumns
        }

      }).flatten :: l
    }).flatten.distinct
}

class ForWorkbooks extends DocumentResolver with JsonWrapper {

  override val resolverType = "datameer"

  override def resolve(repo: String, fullFileName: String): Iterable[Map[String, Any]] = {

    val material: JValue = super.deserialize(FileWrapper.read(fullFileName))

    val subSheets: List[JValue] =
      (material \ "sheets").children.map(sheet =>
        sheet merge super.mapToJValue(Map(
          "repo" -> repo,
          "jsonFilePath" -> fullFileName,
          "thisWorkbookFullName" ->
            PathParser(material \ "file" \ "path": String)(m => m.fullFileName)
        ))
      )

    val resolved: List[List[Map[String, Any]]] =
      subSheets.foldRight(List[List[Map[String, Any]]]())((sheet, remain) => {

        (sheet match {
          case SheetBy2MembersJoin(sheetBy2MembersJoinMatched) => sheetBy2MembersJoinMatched
          case SheetFromReferencedSheet(sheetWithReferencedSheetMatched) => sheetWithReferencedSheetMatched
          case SheetByMultiJoin(sheetByMultiJoinMatched) => sheetByMultiJoinMatched
          case SheetByUnion(sheetByUnionMatched) => sheetByUnionMatched
          case SheetFromDatasource(sheetWithDatasourceMatched) => sheetWithDatasourceMatched
          case SheetByFormulas(sheetWithFormulasMatched) => sheetWithFormulasMatched
          case _ =>
            val errorSheetName: String = sheet \ "name"
            log.error(s"sheet $errorSheetName matches nothing in $fullFileName")
            List[Map[String, Any]]()
        }).filter(MalformedVertexFilter(_)) :: remain
      })

    resolved.flatten
  }
}

object SheetFromDatasource extends JsonWrapper {

  def unapply(materialOfSheet: JValue): Option[List[Map[String, Any]]] =
    materialOfSheet \ "datasource" match {
      case JObject(m) => Some(this.resolve(materialOfSheet))
      case _ => None
    }

  def resolve(materialOfSheet: JValue): List[Map[String, Any]] = {

    val repo: String = materialOfSheet \ "repo"
    val fullFileName: String = materialOfSheet \ "jsonFilePath"
    val gameName: String = GameNameParser(fullFileName)

    val workbookName: String = List(
      gameName,
      materialOfSheet \ "thisWorkbookFullName": String
    ).mkString("/")
    val importVertexName: String = List(
      gameName,
      PathParser(materialOfSheet \ "datasource" \ "path")(m => m.fullFileName)
    ).mkString("/")
    val sheetVertexName: String = List(
      gameName,
      materialOfSheet \ "thisWorkbookFullName": String,
      materialOfSheet \ "name": String
    ).mkString("/")

    val vertex = new Vertex(repo, fullFileName, materialOfSheet)
    List(
      vertex.generateVertex(Map(
        "name" -> importVertexName,
        "vgs" -> List(workbookName),
        "class" -> DataClass.DatameerImportJob,
        "links" -> List(sheetVertexName)
      )),
      vertex.generateVertex(Map(
        "name" -> sheetVertexName,
        "vgs" -> List(workbookName),
        "class" -> DataClass.DatameerSheet,
        "links" -> List()
      ))
    )
  }
}

object SheetFromReferencedSheet extends JsonWrapper {

  def unapply(materialOfSheet: JValue): Option[List[Map[String, Any]]] =

    materialOfSheet \ "referencedSheet" match {
      case JObject(m) => Some(this.resolve(materialOfSheet))
      case _ => None
    }

  def resolve(materialOfSheet: JValue): List[Map[String, Any]] = {

    val repo: String = materialOfSheet \ "repo"
    val fullFileName: String = materialOfSheet \ "jsonFilePath"
    val gameName: String = GameNameParser(fullFileName)

    val workbookName: String = materialOfSheet \ "thisWorkbookFullName"
    val sheetName: String = materialOfSheet \ "name"
    val referenceVertexName: String = List(
      gameName,
      PathParser(materialOfSheet \ "referencedSheet" \ "workbook" \ "path")(m => m.fullFileName),
      materialOfSheet \ "referencedSheet" \ "name": String
    ).mkString("/")

    val sheetVertexName: String =
      List(gameName, workbookName, sheetName).mkString("/")

    val vertex = new Vertex(repo, fullFileName, materialOfSheet)
    List(
      vertex.generateVertex(Map(
        "name" -> referenceVertexName,
        "vgs" -> List(List(gameName, workbookName).mkString("/")),
        "class" -> DataClass.DatameerSheet,
        "links" -> List(sheetVertexName)
      )),
      vertex.generateVertex(Map(
        "name" -> sheetVertexName,
        "vgs" -> List(List(gameName, workbookName).mkString("/")),
        "class" -> DataClass.DatameerSheet,
        "links" -> List()
      ))
    )
  }
}

object SheetByFormulas extends JsonWrapper with Formula {

  def unapply(materialOfSheet: JValue): Option[List[Map[String, Any]]] =
    materialOfSheet \ "formulas" match {
      case JArray(a) => Some(this.resolve(materialOfSheet))
      case _ => None
    }

  def resolve(materialOfSheet: JValue): List[Map[String, Any]] = {

    val repo: String = materialOfSheet \ "repo"
    val fullFileName: String = materialOfSheet \ "jsonFilePath"
    val gameName: String = GameNameParser(fullFileName)

    val workbookName: String = materialOfSheet \ "thisWorkbookFullName"
    val thisSheetName: String = List(
      gameName,
      workbookName,
      materialOfSheet \ "name": String
    ).mkString("/")

    val usedSheetsColumns: Map[String, List[String]] =
      super.getSheetsColumns(materialOfSheet)

    val vertex = new Vertex(repo, fullFileName, materialOfSheet)

    val resolved: List[Map[String, Any]] =
      usedSheetsColumns.keys.foldLeft(List[Map[String, Any]]())((l, beUsedSheet) => {

        val formulaUsedSheetName: String = List(gameName, workbookName, beUsedSheet).mkString("/")
        val columns: Map[String, Map[Nothing, Nothing]] =
          usedSheetsColumns(beUsedSheet) map (eachColumn => (eachColumn -> Map())) toMap

        vertex.generateVertex(Map(
          "name" -> formulaUsedSheetName,
          "vgs" -> List(List(gameName, workbookName).mkString("/")),
          "class" -> DataClass.DatameerSheet,
          "links" -> List(thisSheetName),
          "columns" -> columns
        )) :: l
      })

    vertex.generateVertex(Map(
      "name" -> thisSheetName,
      "vgs" -> List(List(gameName, workbookName).mkString("/")),
      "class" -> DataClass.DatameerSheet,
      "links" -> List()
    )) :: resolved
  }

}

object SheetByUnion extends JsonWrapper {

  def unapply(materialOfSheet: JValue): Option[List[Map[String, Any]]] =

    materialOfSheet \ "sheetDefinition" \ "unionSources" match {
      case JArray(a) => Some(this.resolve(materialOfSheet))
      case _ => None
    }

  def resolve(materialOfSheet: JValue): List[Map[String, Any]] = {

    val repo: String = materialOfSheet \ "repo"
    val fullFileName: String = materialOfSheet \ "jsonFilePath"
    val gameName: String = GameNameParser(fullFileName)

    val thisSheetName: String = List(
      gameName,
      materialOfSheet \ "thisWorkbookFullName": String,
      materialOfSheet \ "name": String
    ).mkString("/")
    val workbookName: String = materialOfSheet \ "thisWorkbookFullName"

    val usedSheets: List[String] =
      for (unionSheetElement <- (materialOfSheet \ "sheetDefinition" \ "unionSources").children) yield {
        unionSheetElement: String
      }

    val vertex = new Vertex(repo, fullFileName, materialOfSheet)

    val resolved: List[Map[String, Any]] =
      usedSheets.foldRight(List[Map[String, Any]]())((beUsedSheet, remain) => {

        val joinedSheetName: String =
          List(gameName, workbookName, beUsedSheet).mkString("/")

        vertex.generateVertex(Map(
          "name" -> joinedSheetName,
          "vgs" -> List(List(gameName, workbookName).mkString("/")),
          "class" -> DataClass.DatameerSheet,
          "links" -> List(thisSheetName)
        )) :: remain
      })

    vertex.generateVertex(Map(
      "name" -> thisSheetName,
      "vgs" -> List(List(gameName, workbookName).mkString("/")),
      "class" -> DataClass.DatameerSheet,
      "links" -> List()
    )) :: resolved
  }
}

object SheetByMultiJoin extends JsonWrapper with Join {

  def unapply(materialOfSheet: JValue): Option[List[Map[String, Any]]] =
    (materialOfSheet \ "sheetDefinition" \ "joinCategory": String) match {
      case "MULTI_JOIN" => Some(this.resolve(materialOfSheet))
      case _ => None
    }

  def resolve(materialOfSheet: JValue): List[Map[String, Any]] = {

    val repo: String = materialOfSheet \ "repo"
    val fullFileName: String = materialOfSheet \ "jsonFilePath"
    val gameName: String = GameNameParser(fullFileName)

    val thisSheetName: String = List(
      gameName,
      materialOfSheet \ "thisWorkbookFullName": String,
      materialOfSheet \ "name": String
    ).mkString("/")
    val workbookName: String = materialOfSheet \ "thisWorkbookFullName"

    val usedSheetsColumns: Map[String, List[String]] =
      super.getDownStreamSheetsColumns(materialOfSheet)

    val vertex = new Vertex(repo, fullFileName, materialOfSheet)

    val resolved: List[Map[String, Any]] =
      usedSheetsColumns.keys.foldLeft(List[Map[String, Any]]())((l, beUsedSheet) => {

        val joinedSheetName: String =
          List(gameName, workbookName, beUsedSheet).mkString("/")

        val columns: Map[String, Map[Nothing, Nothing]] =
          usedSheetsColumns(beUsedSheet) map (s => (s -> Map())) toMap

        vertex.generateVertex(Map(
          "name" -> joinedSheetName,
          "vgs" -> List(List(gameName, workbookName).mkString("/")),
          "class" -> DataClass.DatameerSheet,
          "links" -> List(thisSheetName),
          "columns" -> columns
        )) :: l
      })

    vertex.generateVertex(Map(
      "name" -> thisSheetName,
      "vgs" -> List(List(gameName, workbookName).mkString("/")),
      "class" -> DataClass.DatameerSheet,
      "links" -> List(),
      "columns" -> super.getSheetsColumnsInMyself(materialOfSheet)
    )) :: resolved
  }
}

object SheetBy2MembersJoin extends JsonWrapper with Join {

  def unapply(materialOfSheet: JValue): Option[List[Map[String, Any]]] =

    (materialOfSheet \ "sheetDefinition" \ "joinCategory": String) match {
      case "TWO_MEMBER_JOIN" => Some(this.resolve(materialOfSheet))
      case _ => None
    }

  def resolve(materialOfSheet: JValue): List[Map[String, Any]] = {

    val repo: String = materialOfSheet \ "repo"
    val fullFileName: String = materialOfSheet \ "jsonFilePath"
    val gameName: String = GameNameParser(fullFileName)

    val thisSheetName: String = List(
      gameName,
      materialOfSheet \ "thisWorkbookFullName": String,
      materialOfSheet \ "name": String
    ).mkString("/")
    val workbookName: String = materialOfSheet \ "thisWorkbookFullName"

    val usedSheetsColumns: Map[String, List[String]] =
      super.getDownStreamSheetsColumns(materialOfSheet)

    val vertex = new Vertex(repo, fullFileName, materialOfSheet)

    val resolved: List[Map[String, Any]] =
      usedSheetsColumns.keys.foldLeft(List[Map[String, Any]]())((l, beUsedSheet) => {

        val joinedSheetName: String =
          List(gameName, workbookName, beUsedSheet).mkString("/")

        val columns: Map[String, Map[Nothing, Nothing]] =
          usedSheetsColumns(beUsedSheet) map (s => (s -> Map())) toMap

        vertex.generateVertex(Map(
          "name" -> joinedSheetName,
          "vgs" -> List(List(gameName, workbookName).mkString("/")),
          "class" -> DataClass.DatameerSheet,
          "links" -> List(thisSheetName),
          "columns" -> columns
        )) :: l
      })

    vertex.generateVertex(Map(
      "name" -> thisSheetName,
      "vgs" -> List(List(gameName, workbookName).mkString("/")),
      "class" -> DataClass.DatameerSheet,
      "links" -> List(),
      "columns" -> super.getSheetsColumnsInMyself(materialOfSheet)
    )) :: resolved
  }
}

class ForExportJobs extends DocumentResolver with JsonWrapper {

  override val resolverType = "datameer"

  override def resolve(repo: String, fullFileName: String): Iterable[Map[String, Any]] = {

    val material: JValue = super.deserialize(FileWrapper.read(fullFileName))

    (material merge super.mapToJValue(Map(
      "repo" -> repo,
      "jsonFilePath" -> fullFileName
    )) match {

      case ExportJob(exportJobsMatched) => exportJobsMatched
      case _ =>
        val errorWorkbookName: String = material \ "name"
        log.error(s"export job: $errorWorkbookName matches nothing in $fullFileName")
        List[Map[String, Any]]()
    }).filter(MalformedVertexFilter(_))
  }
}

object ExportJob extends JsonWrapper {

  def unapply(material: JValue): Option[Iterable[Map[String, Any]]] =
    (material \ "sheet" \ "workbook" \ "path": String) match {
      case path: String => Some(this.resolve(material))
      case _ => None
    }

  def resolve(material: JValue): Iterable[Map[String, Any]] = {

    val repo: String = material \ "repo"
    val fullFileName: String = material \ "jsonFilePath"
    val gameName: String = GameNameParser(fullFileName)
    val downStreamFullName: String =
      PathParser(material \ "file" \ "path")(m => m.fullFileName)
    val workbookName: String = List(
      gameName,
      PathParser(material \ "sheet" \ "workbook" \ "path")(m => m.fullFileName)
    ).mkString("/")

    val sourceVertexName: String = List(
      workbookName,
      material \ "sheet" \ "name": String
    ).mkString("/")

    val vertex = new Vertex(repo, fullFileName, material)
    List(
      vertex.generateVertex(Map(
        "name" -> List(
          gameName,
          downStreamFullName
        ).mkString("/"),
        "class" -> DataClass.DatameerExportJob,
        "links" -> List(
          List(
            gameName,
            PathParser(material \ "file" \ "path")(m => m.fileName) + ".csv"
          ).mkString("/")
        ),
        "vgs" -> List(workbookName)
      )),
      vertex.generateVertex(Map(
        "name" -> sourceVertexName,
        "class" -> DataClass.DatameerSheet,
        "links" -> List(
          List(
            gameName,
            downStreamFullName
          ).mkString("/")
        ),
        "vgs" -> List(workbookName)
      ))
    )
  }
}

