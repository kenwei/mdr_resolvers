/**
 * Created by bshih on 8/4/15.
 */

package com.machinezone.resolver.datameer.utils

import java.io.{StringWriter, PrintWriter}
import com.machinezone.util.Helper.getRelativePath
import org.slf4j.LoggerFactory
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.native.Serialization

trait JsonWrapper {

  protected def serialize[A <: scala.AnyRef](variousObject: A): String = {

    implicit val formats = org.json4s.DefaultFormats
    Serialization.write(variousObject)
  }

  protected def deserialize(jsonString: String): JValue =
    parse(jsonString)

  protected def mapToJValue(map: Map[String, Any]): JValue = {

    implicit val formats = org.json4s.DefaultFormats
    parse(Serialization.write(map))
  }

  implicit protected def stringExtractor(unpack: JValue): String = {

    implicit val formats = DefaultFormats
    render(unpack) match {
      case JString(s) => s
      case _ => ""
    }
  }
}

object FileWrapper {

  def read(path: String): String =
    scala.io.Source.fromFile(path).mkString
}

case class PathHolder(val filePath: String, val fileName: String, val fileType: String) {
  def fullFileName: String = List(fileName, fileType).mkString(".")
}
object PathParser {

  val pattern = """(/.*/)*(.+)\.(.+)""".r
  def matcher(fullFilePath: String): Option[PathHolder] = fullFilePath match {
    case pattern(filePath, fileName, fileType) => Some(PathHolder(filePath, fileName, fileType))
    case _ => None
  }

  lazy val log = LoggerFactory.getLogger(this.getClass)
  def apply(fullFileName: String)(f: PathHolder => String) =
    matcher(fullFileName).map(f).getOrElse({
      val e: Exception = new Exception
      log.warn(s"Path parse fail: $fullFileName", e)
      ""
    })
}

object GameNameParser {

  def apply(fullFileName: String): String = {

    val pattern = "^.*?wiso.*?$".r
    fullFileName.split("/").takeRight(3).mkString("/") match {
      case pattern() => "wiso"
      case _ => "ody"
    }
  }
}

class Vertex(repo: String, fullFileName: String, material: JValue) {

  def getGeneralProperties: Map[String, Any] =
    Map(
      "src_files" -> List(
        getRelativePath(fullFileName, repo, include = true)
      ),
      "columns" -> GetColumns(material)
    )

  def generateVertex(m: Map[String, Any]): Map[String, Any] = {

    val generalProperties: Map[String, Any] =
      getGeneralProperties.filter(eachVertex => {
        eachVertex._1 match {
          case "columns" =>
            eachVertex._2.asInstanceOf[Map[String, Map[Nothing, Nothing]]].values.nonEmpty
          case _ => true
        }
      })

    m.keySet ++ generalProperties.keySet map { k =>
      k -> {
        m.getOrElse(k, generalProperties.get(k))
      }
    } toMap
  }
}

object MalformedVertexFilter {

  def apply(m: Map[String, Any]): Boolean = {
    m.forall(term => term match {
      case (k, v) => k match {
        case "name" => v.toString.split("""/|\.""").forall(x => x.nonEmpty)
        case "links" =>
          v.asInstanceOf[List[String]].forall(_.split("""/|\.""").forall(x => x.nonEmpty))
        case _ => true
      }
    })
  }
}

object GetColumns extends JsonWrapper {

  def getFirstNonEmpty(m: JValue, keyList: List[String]): JValue = keyList match {
    case key :: tail if (m \ key).toSome.nonEmpty => m \ key
    case key :: tail => getFirstNonEmpty(m, tail)
    case Nil => JNothing
  }

  /**
   * Order of elements in List columnDataKeyCandidates and
   * columnNameKeyCandidates are priority sensitive.
   */
  val columnDataKeyCandidates: List[String] = List(
    "columnStyles",
    "mappings"
  )

  val columnNameKeyCandidates: List[String] = List(
    "name",
    "columnId"
  )

  def apply(material: JValue): Map[String, Any] = {

    val rawColumnsData: JValue = getFirstNonEmpty(material, columnDataKeyCandidates)

    rawColumnsData.children.foldLeft(Map[String, Any]()) { (m, eachColumn) =>

      val columnName: String = getFirstNonEmpty(eachColumn, columnNameKeyCandidates)

      m ++ Map(
        columnName -> Map()
      )
    }
  }
}
