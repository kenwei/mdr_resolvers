/**
 * Created by bshih on 8/27/15.
 */
package com.machinezone.resolver

import akka.actor.{ActorRef, Props}
import com.machinezone.resolver.datameer.extractors.{ForExportJobs, ForWorkbooks}

class DatameerResolver extends RepositoryResolver {

  override def getDocumentResolver(filename: String): ActorRef = {

    filename.split("/").takeRight(2)(0) match {
      case "exportjob" => context.actorOf(Props[ForExportJobs])
      case "workbook" => context.actorOf(Props[ForWorkbooks])
      case _ =>
        log.warning(s"Found unresolvable file -> $filename")
        context.actorOf(Props.empty)
    }
  }
}
