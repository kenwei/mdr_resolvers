import com.typesafe.sbt.SbtNativePackager.autoImport.NativePackagerKeys._
import com.typesafe.sbt.SbtNativePackager.{Docker => DKer}
import com.typesafe.sbt.packager.docker._

object Docker {

  lazy val settings = Seq(

    packageName in DKer := packageName.value,

    dockerUpdateLatest in DKer := true,

    defaultLinuxInstallLocation in DKer := "/opt/docker",

    dockerBaseImage := "java:8",

    dockerExposedVolumes += "/data",

    dockerCommands := dockerCommands.value.filterNot {

      // ExecCmd is a case class, and args is a varargs variable, so you need to bind it with @
      case ExecCmd("ENTRYPOINT", args @ _*) => true

      // dont filter the rest
      case cmd                       => false
    },
    dockerCommands ++= Seq(
      Cmd("ENV", "APP_ENV docker"),
      ExecCmd("RUN", "sed", "-i", "s#./output#/data/output/#g", "conf/etl-documents.json"),
      ExecCmd("RUN", "sed", "-i", "s#./databases/#/data/databases/#g", "conf/etl-documents.json"),
      ExecCmd("RUN", "sed", "-i", "s#./output#/data/output/#g", "conf/etl-edges.json"),
      ExecCmd("RUN", "sed", "-i", "s#./databases/#/data/databases/#g", "conf/etl-edges.json"),
      ExecCmd("CMD", "/bin/bash")
    )
  )

}
