import java.io.File.{separator => FS}

import org.javacc.jjtree.JJTree
import org.javacc.parser.Main
import sbt.{File, FileFilter, PathFinder, RichFile}

object Tasks {

  def generateParser(src:RichFile, out:RichFile, pkgName:String):Seq[File] = {
    val targetFolder = out.absolutePath + FS + pkgName.replaceAll("\\.", FS)

    def jjtFiles = (PathFinder(src / "jjtree") ** FileFilter.globFilter("*.jjt")).get.map(_.getAbsolutePath)
    new JJTree().main(Array("-GRAMMAR_ENCODING=UTF-8",
                            "-JDK_VERSION=1.8",
                            "-VISITOR=true",
                            "-NODE_DEFAULT_VOID=true",
                            "-TRACK_TOKENS=true",
                            "-OUTPUT_DIRECTORY=" + targetFolder) ++ jjtFiles)

    def jjFiles = (PathFinder(new File(targetFolder)) ** FileFilter.globFilter("*.jj")).get.map(_.getAbsolutePath)
    Main.mainProgram(Array("-GRAMMAR_ENCODING=UTF-8",
                           "-OUTPUT_DIRECTORY=" + targetFolder) ++ jjFiles)
    (PathFinder(out.asFile) ** FileFilter.globFilter("*.java")).get
  }

}