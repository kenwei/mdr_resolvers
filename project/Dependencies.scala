import sbt._

object Dependencies {

  val akkaVersion = "2.4-SNAPSHOT"
  val orientDBVersion = "2.1.1"

  lazy val akka = Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test"
  )

  lazy val graph = Seq(
    "com.michaelpollmeier" %% "gremlin-scala" % "3.0.0-incubating",
    "com.ansvia.graph" %% "blueprints-scala" % "0.1.61-20150416-SNAPSHOT" excludeAll(
      ExclusionRule(organization = "com.tinkerpop"),
      ExclusionRule(organization = "com.tinkerpop.gremlin"),
      ExclusionRule(organization = "com.tinkerpop.bluprints")
    ),
    "com.tinkerpop.blueprints" % "blueprints-core" % "2.6.0"
  )

  lazy val parser = Seq(
    "org.scala-lang.modules" %% "scala-xml" % "1.0.2",
    "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.2"
  )

  lazy val jsqlParser = Seq(
    "net.java.dev.javacc" % "javacc" % "6.1.2",
    "com.github.jsqlparser" % "jsqlparser" % "0.9.4"
  )

  lazy val orientdb = Seq(
    "commons-io" % "commons-io" % "2.4",
    "com.orientechnologies" % "orientdb-core" % orientDBVersion,
    "com.orientechnologies" % "orientdb-client" % orientDBVersion,
    "com.orientechnologies" % "orientdb-enterprise" % orientDBVersion,
    "com.orientechnologies" % "orientdb-graphdb" % orientDBVersion,
    "com.orientechnologies" % "orientdb-etl" % orientDBVersion,
    "com.tinkerpop.blueprints" % "blueprints-core" % "2.6.0"
  )

  lazy val jgit = Seq(
    "org.eclipse.jgit" % "org.eclipse.jgit" % "4.0.1.201506240215-r"
  )

  lazy val slf4j = Seq(
    "org.slf4j" % "slf4j-simple" % "1.6.4"
  )

  lazy val test = Seq(
    "org.scalatest" %% "scalatest" % "2.2.5" % "test"
  )

  lazy val jerkson = Seq(
    "com.gilt" %% "jerkson" % "0.6.6"
  )

  lazy val datameerResolver = Seq(
    "org.json4s" %% "json4s-native" % "3.2.11",
    "org.json4s" %% "json4s-jackson" % "3.2.11"
  )

  lazy val jython = Seq(
    "org.python" % "jython-standalone" % "2.7.0"
  )

  lazy val files = Seq(
    "com.github.pathikrit" %% "better-files" % "2.8.1"
  )

}