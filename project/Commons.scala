import sbt._
import sbt.Keys._

object Commons {

  lazy val settings = Seq(
    version := "1.0.0",
    organization := "com.machinezone",
    scalaVersion := "2.11.7",

    resolvers ++= Seq(
      "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/",
      "Sonatype repo" at "https://oss.sonatype.org/content/repositories/snapshots",
      "jgit-repo" at "http://download.eclipse.org/jgit/maven",
      Resolver.jcenterRepo
    )
  )

  lazy val javaSettings = Seq(
    fork := true,
    javacOptions ++= Seq(
      "-source", "1.8",
      "-target", "1.8",
      "-encoding", "UTF-8"
    ),
    crossPaths := false,
    autoScalaLibrary := false,
    sources in doc in Compile := List(),
    testOptions += Tests.Argument(TestFrameworks.JUnit, "-q", "-v")
  )

}