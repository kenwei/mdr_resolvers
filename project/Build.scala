import com.typesafe.sbt.packager.archetypes.JavaAppPackaging
import net.virtualvoid.sbt.graph.Plugin.graphSettings
import com.typesafe.sbt.packager.docker.DockerPlugin
import sbt.Keys
import sbt._
import Keys._

/**
 * based on https://github.com/harrah/xsbt/wiki/Getting-Started-Multi-Project
 */
object HappyScalaBuild extends Build {

  // aggregate: running a task on the aggregate project will also run it on the aggregated projects.
  // dependsOn: a project depends on code in another project.
  lazy val root = Project(
    id = "happyscala",
    base = file("."),
    settings = Commons.settings ++ Seq(
      mainClass in Compile := Some("HappyScala"),
      libraryDependencies ++= Dependencies.akka ++
                              Dependencies.test,
      cleanFiles += file("output"),
      fork := true,
      outputStrategy := Some(StdoutOutput),
      javaOptions in run += "-Dconfig.file=./src/universal/conf/app.conf"
    ) ++ graphSettings ++ Docker.settings,
    aggregate = Seq(core, sqlParser, tableauResolver, datameerResolver, verticaResolver, orientDBETL)
  ).dependsOn(core, sqlParser, tableauResolver, datameerResolver, verticaResolver, orientDBETL)
   .enablePlugins(DockerPlugin)
   .enablePlugins(JavaAppPackaging)

  // sub-project in the Resolver subdirectory
  lazy val core = Project(
    id = "happyscala-core",
    base = file("Core"),
    settings = Commons.settings ++ Seq(
      libraryDependencies ++= Dependencies.files ++
                              Dependencies.akka ++
                              Dependencies.parser ++
                              Dependencies.jerkson ++
                              Dependencies.jgit ++
                              Dependencies.slf4j ++
                              Dependencies.test
    )
  )

  // sub-project in the JSqlParser subdirectory
  lazy val sqlParser = Project(
    id = "jsqlparser",
    base = file("JSqlParser"),
    settings = Commons.settings ++ Seq(
      libraryDependencies ++= Dependencies.jsqlParser ++
                              Dependencies.test,
      sourceGenerators in Compile <+=
        (sourceDirectory in Compile, sourceManaged in Compile) map { (src, out) =>
        Tasks.generateParser(src, out, "net.sf.jsqlparser.parser")
      }
    )
  )

  // sub-project in the TableauResolver subdirectory
  lazy val tableauResolver = Project(
    id = "happyscala-tableau-resolver",
    base = file("TableauResolver"),
    settings = Commons.settings ++ Seq(
      libraryDependencies ++= Dependencies.graph ++
                              Dependencies.test
    )
  ).dependsOn(core, sqlParser)

  // sub-project for OrientDB ETL
  lazy val orientDBETL = Project(
    id = "orientdb-etl",
    base = file("OrientDBETL"),
    settings = Commons.settings ++ Seq(
      libraryDependencies ++= Dependencies.orientdb ++
                              Dependencies.test
    )
  )

  // sub-project in the DatameerResolver subdirectory
  lazy val datameerResolver = Project(
    id = "happyscala-datameer-resolver",
    base = file("DatameerResolver"),
    settings = Commons.settings ++ Seq(
      libraryDependencies ++= Dependencies.datameerResolver ++
                              Dependencies.test
    )
  ).dependsOn(core)


  // sub-project in the datameerResolver subdirectory
  lazy val verticaResolver = Project(
    id = "happyscala-vertica-resolver",
    base = file("VerticaResolver"),
    settings = Commons.settings ++ Seq(
      libraryDependencies ++= Dependencies.jython
    )
  ).dependsOn(core)


}
