### Set up local development environment

After you checked out the latest repo, run ```bootstrap.sh``` to download all dependencies and place them in default places.

To create a OrientDB database with resolved meta data of our data pipeline, run ```APP_ENV=dev sbt run```. The command should generate a database under ```databases/MDR```. Copy this folder to your OrientDB database directory if you prefer to browse it with OrientDB studio.

NOTE: This database will be wiped out and regenerated every time by default, so please remember saving changes you made to somewhere else or revise ```app.conf```/```etl-documents.json``` to suit your needs.

### Run tests

Simply run ```APP_ENV=test sbt test```.

### How to run in a docker container

You have to run your boot2docker first, and then build docker image by following command:

```
$sbt docker:publishLocal
```

Mount your repo volume by replacing **{PATH_TO_*}** to run the docker container

```
$docker run \
    --name=happy -d -i \
	-v {PATH_TO_BI_TABLEAU_REPO}:/data/deps/bi_tableau \
	-v {PATH_TO_BI_ANALYTICS_REPO}:/data/deps/bi_analytics \
	happyscala
```

Execute happyscala to run static analysis:

```
$docker exec -it happy bin/happyscala
```

The output would be placed in `/data` in the container, you can ssh to container to double check:

```
$docker exec -it happy /bin/bash
$cd /data
$ls -al
```
```
total 20
drwxr-xr-x  5 daemon daemon 4096 Aug 25 01:33 .
drwxr-xr-x 63 root   root   4096 Aug 25 01:30 ..
drwxr-xr-x  3 root   root   4096 Aug 25 01:33 databases
drwxr-xr-x  4 root   root   4096 Aug 25 01:30 deps
drwxr-xr-x  5 root   root   4096 Aug 25 01:30 output
```